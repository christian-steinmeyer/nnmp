import os
import pandas as pd

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

BALANCE_STRATEGY_REPR = 'balance_strategy'
BALANCE_STRATEGY_ABBREVIATION = 'ba'
DEFAULT_TIME = pd.to_timedelta('00:00:00')
ID_REPR = 'ID'
IRRELEVANT_OUTCOME_DATA = ['SAPS-I', 'SOFA', 'Length_of_stay', 'Survival']
RECORD_ID_REPR = 'RecordID'
SPLIT_REPR = 'split'
TIME_REPR = 'time'
