# Physionet Challenge 2012

## Prerequisites

After making sure to have the correct conda environment (see top level readme), download all relevant files from [the official website](https://www.physionet.org/content/challenge-2012/1.0.0/). You should have the following structure of files in [origianl_data](original_data):

- `set-a`
  - `135365.txt`
  - ...
- `set-b`
  - ...
- `set-c`
  - ...
- `Outcomes-a.txt`
- `Outcomes-b.txt`
- `Outcomes-c.txt`

## Usage

Run the notebooks in the following order:

1. data_pre_processsing
1. data_feature_extraction
1. model

Note, that they were separated into multiple files for better readability and performance.