from typing import Type

import keras.backend as keras
from imblearn.base import BaseSampler
from imblearn.under_sampling import RandomUnderSampler
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.feature_selection import chi2
from sklearn.preprocessing import MinMaxScaler

from physionetChallenge.src.AbstractModelEvaluation import AbstractModelEvaluation
from physionetChallenge.src.DataFactory import *
from src.utils import *


def _build_model_parameters(parameter_map: ParameterMap):
    result = {}
    representation = ''
    for key, value in parameter_map.name_values_map.items():
        if len(value) is 1:
            representation += f'_{parameter_map.get_abbreviation(key)}={value[0]}'
            result[key] = value[0]
        else:
            raise RuntimeError(f'The attribute {key} was assigned {len(value)} values, which is ambiguous')
    return representation, result


class SingleModelEvaluation(AbstractModelEvaluation):
    """
    Convenience class that can perform a single model evaluation on preprocessed data.
    """

    def __init__(self, number_of_features: int = None, scaler: (BaseEstimator, TransformerMixin) = MinMaxScaler(),
                 ensemble_size: int = 7, parameter_map: ParameterMap = None, training_data_factory: DataFactory = None,
                 test_data_factory: DataFactory = None, balance_strategy: Type[BaseSampler] = RandomUnderSampler):
        super().__init__(number_of_features, scaler, ensemble_size, parameter_map, training_data_factory)
        self.model_name, self.parameters = _build_model_parameters(parameter_map)
        self.test_data_factory = test_data_factory
        self.sampler = balance_strategy(random_state=self.random_state)

        # needs setting through add_data
        self.test_data = None
        self.test_target = None

    def set_data(self, target_variable_name: str = 'In-hospital_death') -> None:
        """
        Sets data and targets for training and testing through data available through training and test data factories.

        Parameters
        ----------
        target_variable_name : str
            The name of the column which includes the target variable
        """
        super().set_data(target_variable_name)
        self.test_target, self.test_data = self._set_data(self.test_data_factory, target_variable_name)

    def select_features(self, score_func=chi2) -> None:
        """
        Reduces the set of features to self.number_of_features for both, training and testing data through the given
        scoring function.

        Parameters
        ----------
        score_func : func
            A scikit-learn compatible scoring function taking two parameters:
            X: {array-like, sparse matrix} of shape (n_samples, n_features)
                Sample vectors.
            y: array-like of shape (n_samples,)
                Target vector (class labels).
            and returning
            array, shape = (n_features,)
                score for each feature in given order
        """
        super().select_features(score_func)
        self.test_data = self._select_features(score_func, self.test_data, self.test_target)

    def run(self, n_jobs: int = -1) -> None:
        """
        Performs the actual grid search using multiple processes, if desired.

        Parameters
        ----------
        n_jobs : int
            The number of processes to be used, for the number of available processors use -1.
        """
        self.X, self.X_test, self.y, self.y_test = self.data, self.test_data, self.target, self.test_target
        self.sample_data(self.sampler)
        result = self.fit_model(self.parameters, self.model_name)
        print(result)
        keras.clear_session()
