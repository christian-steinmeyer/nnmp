import logging
from multiprocessing import freeze_support

from imblearn.over_sampling import RandomOverSampler
from imblearn.over_sampling import SMOTE

from physionetChallenge.src.GridSearch import *
from physionetChallenge.src.NeuralNetwork import NeuralNetworkParameterKeys
from physionetChallenge.src.ParameterMap import ParameterMap, Parameter, DefaultParameterKeys
from physionetChallenge.src.PlotFactory import PlotFactory


def perform_grid_search():
    # number of features
    n_features = 20
    parameters = [Parameter(NeuralNetworkParameterKeys.NETWORK_DEPTH, [1, 2, 3, 4, 5, 6, 7]),
                  Parameter(NeuralNetworkParameterKeys.ACTIVATION_FUNCTION, ['relu']),
                  Parameter(NeuralNetworkParameterKeys.OPTIMIZER, ['adam']),
                  Parameter(NeuralNetworkParameterKeys.DROP_RATE, [0.0, 0.001, 0.01, 0.1]),
                  Parameter(NeuralNetworkParameterKeys.INITIALIZER, ['RandomNormal']),
                  Parameter(NeuralNetworkParameterKeys.NETWORK_WIDTH,
                            [n_features, n_features * 2, n_features * 5, n_features * 10]),
                  Parameter(NeuralNetworkParameterKeys.NETWORK_DEPTH.INPUT_DIMENSIONS, [n_features]),
                  Parameter(NeuralNetworkParameterKeys.LAST_ACTIVATION_FUNCTION, ['softmax'])]

    parameter_map = ParameterMap(parameters)

    data_factory = DataFactory(data_set='a', project='physionetChallenge')

    grid_search = GridSearch(number_of_features=n_features, parameter_map=parameter_map, data_factory=data_factory)
    grid_search.set_data(target_variable_name='In-hospital_death')
    grid_search.select_features()
    grid_search.add_balance_strategy('under', RandomUnderSampler)
    grid_search.add_balance_strategy('over', RandomOverSampler)
    grid_search.add_balance_strategy('smote', SMOTE)
    grid_search.run()
    return data_factory.get_grid_search_data()


def plot_results(data):
    plot_factory = PlotFactory(data=data)
    parameters = [NeuralNetworkParameterKeys.NETWORK_DEPTH, NeuralNetworkParameterKeys.NETWORK_WIDTH,
                  NeuralNetworkParameterKeys.DROP_RATE, DefaultParameterKeys.BALANCE_STRATEGY]
    plot_factory.boxplot_summary(parameters=parameters, title='Evaluation Results')
    plot_factory.correlation_summary()


if __name__ == '__main__':
    logging.basicConfig(level='INFO')
    freeze_support()
    results = perform_grid_search()
    plot_results(results)
