import itertools
from logging import debug
from typing import Type, Mapping, Any

import keras.backend as keras
from imblearn.base import BaseSampler
from imblearn.under_sampling import RandomUnderSampler
from joblib import Parallel, delayed
from numpy.core.multiarray import ndarray
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.preprocessing import MinMaxScaler

from physionetChallenge.src.AbstractModelEvaluation import AbstractModelEvaluation
from physionetChallenge.src.DataFactory import *
from physionetChallenge.src.ParameterMap import ParameterMap
from src.utils import *


def _build_parameter_grid(parameter_map: ParameterMap) -> dict:
    """
    Builds a parameter grid from a given dictionary of parameters.

    Parameters
    ----------
    parameter_map : ParameterMap
        The map parameters that the grid should be built from.
    Returns
    -------
    dict
        The parameter grid consists of all possible combinations of the given parameters, given in the form of a dict,
        where keys are a representation of the combination and the value is a single combination.
    """
    parameters = parameter_map.name_values_map
    models = {}
    keys = parameters.keys()
    values = (parameters[key] for key in keys)
    combination_dicts = [dict(zip(keys, combination)) for combination in itertools.product(*values)]
    for combination in combination_dicts:
        representation = ''
        for name, value in combination.items():
            representation += f'_{parameter_map.get_abbreviation(name)}={value}'
        models[representation] = combination
    return models


class GridSearch(AbstractModelEvaluation):
    """
    Convenience class that can perform a grid search over hyper parameters on preprocessed data.
    """

    def __init__(self, number_of_features: int = None, number_of_splits: int = 10, number_of_repeats: int = 2,
                 scaler: (BaseEstimator, TransformerMixin) = MinMaxScaler(), ensemble_size: int = 7,
                 parameter_map: ParameterMap = None, data_factory: DataFactory = None):
        super().__init__(number_of_features, scaler, ensemble_size, parameter_map, data_factory)
        self.rskf = RepeatedStratifiedKFold(n_splits=number_of_splits, n_repeats=number_of_repeats,
                                            random_state=self.random_state)
        self.models = _build_parameter_grid(parameter_map)

        # needs setting through add_balance_strategy
        self.balance_strategies = {}

    def add_balance_strategy(self, name: str, sampler: Type[BaseSampler] = RandomUnderSampler) -> None:
        """
        Adds a balance strategy to the grid search.

        Parameters
        ----------
        name : str
            Name of the balance strategy
        sampler : Type[BaseSampler]
            scikit-learn compatible implementation of the balance strategy
        """
        if not name:
            raise AttributeError(f'Cannot add balance strategy without a name.')
        self.balance_strategies[name] = sampler(random_state=self.random_state)

    def fit_model(self, parameters: Mapping[str, Any], model_representation: str,
                  use_cache: bool = False) -> pd.Series:
        if self.data_factory.model_is_already_evaluated(model_representation):
            info(f'skipping {model_representation}')
            return pd.Series()
        else:
            info(f'evaluating {model_representation}')
            super().fit_model(parameters, model_representation, use_cache)

    def run(self, n_jobs: int = -1) -> None:
        """
        Performs the actual grid search using multiple processes, if desired.

        Parameters
        ----------
        n_jobs : int
            The number of processes to be used, for the number of available processors use -1.
        """
        for balance_strategy, sampler in self.balance_strategies.items():
            split = 0
            info(balance_strategy)
            for train_index, test_index in self.rskf.split(self.data, self.target):
                try:
                    split += 1
                    self.split_data_for_iteration(train_index, test_index)
                    self.sample_data(sampler)
                    if self.was_not_yet_evaluated(balance_strategy, split):
                        info(f'starting pool for split {split}')
                        results = Parallel(n_jobs=n_jobs)(
                            delayed(self.fit_model)(parameters,
                                                    get_model_representation(model_name, balance_strategy, split),
                                                    use_cache=True) for model_name, parameters in self.models.items())

                        for result in results:
                            debug(result)
                        self.data_factory.clean_grid_search_cache(
                            [get_model_representation(model_name, balance_strategy, split) for model_name in
                             self.models.keys()])

                    keras.clear_session()
                except:
                    self.data_factory.clean_grid_search_cache(
                        [get_model_representation(model_name, balance_strategy, split) for model_name in
                         self.models.keys()])

    def was_not_yet_evaluated(self, balance_strategy: str, split: int) -> bool:
        """
        If any of the models for the given balance strategy and split were not yet evaluated, returns true.

        Parameters
        ----------
        balance_strategy : str
            Name of the balance strategy
        split : int
            Split identifier (count)
        Returns
        -------
        bool
            True, if the given balance strategy and split was not yet fully evaluated, False otherwise.
        """
        for model_name, parameters in self.models.items():
            model_representation = get_model_representation(model_name, balance_strategy, split)
            if self.data_factory.model_is_already_evaluated(model_representation):
                info(f'skipping {model_representation}')
            else:
                return True
        return False

    def split_data_for_iteration(self, train_index: ndarray, test_index: ndarray):
        """
        Analogously to scikit-learn traintestsplit, this method splits the available data into train and test data for
        the given indices.

        Parameters
        ----------
        train_index : ndarray
            Index used for training data (no overlap with test_index)
        test_index : ndarray
            Index used for test data (no overlap with train_index)
        """

        self.X, self.X_test = self.data.iloc[train_index], self.data.iloc[test_index]
        self.y, self.y_test = self.target.iloc[train_index], self.target.iloc[test_index]
