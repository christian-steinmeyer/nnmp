import logging

from physionetChallenge.src.DataFactory import DataFactory
from physionetChallenge.src.PreProcessor import PreProcessor, PreProcessorType


def perform_pre_processing():
    logging.basicConfig(level='INFO')
    create_plots = True
    outlier_strategy = None

    data_factory = DataFactory(data_set='a', project='physionetChallenge', outlier_strategy=outlier_strategy)
    pre_processor = PreProcessor(data_factory, PreProcessorType.TRAINING, create_plots)

    pre_processor.remove_erroneous_values()
    pre_processor.apply_transformations()
    variable_summaries = pre_processor.get_variable_summaries()
    pre_processor.impute_data()
    pre_processor.remove_outliers()
    pre_processor.extract_features()

    test_data_factories = [
        DataFactory(data_set=data_set, project='physionetChallenge', outlier_strategy=outlier_strategy) for data_set in
        ['b', 'c']]
    test_pre_processors = [PreProcessor(factory, PreProcessorType.TESTING) for factory in test_data_factories]
    for test_pre_processor in test_pre_processors:
        test_pre_processor.remove_erroneous_values()
        test_pre_processor.apply_transformations()
        test_pre_processor.impute_data(variable_summaries)
        test_pre_processor.remove_outliers()
        test_pre_processor.extract_features()


if __name__ == '__main__':
    perform_pre_processing()
