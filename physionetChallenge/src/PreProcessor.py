import functools
from enum import Enum, auto
from logging import info, warning
from typing import Callable, Any, Tuple, Union, List

import numpy as np
import pandas as pd

from constants import ID_REPR, TIME_REPR, DEFAULT_TIME, IRRELEVANT_OUTCOME_DATA, RECORD_ID_REPR
from physionetChallenge.src.DataFactory import DataFactory, PipelineStep
from physionetChallenge.src.ImputationMethod import ImputationMethod
from physionetChallenge.src.PlotFactory import PlotFactory
from physionetChallenge.src.Variable import Variable
from physionetChallenge.src.VariableCategory import VariableCategory

# set seed to achieve predictability
np.random.seed(42)


def conjunction(*conditions: Any) -> Any:
    """
    Convenience method to combine multiple conditions through a logical and operation.
    """
    return functools.reduce(np.logical_and, conditions)


def disjunction(*conditions: Any) -> Any:
    """
    Convenience method to combine multiple conditions through a logical or operation.
    """
    return functools.reduce(np.logical_or, conditions)


class PreProcessorType(Enum):
    """
    Representation of the pre processor type.
    """
    TRAINING = auto()
    TESTING = auto()

    def __repr__(self):
        return f'<{self.__class__.__name__}.{self.name}>'


class PreProcessor:
    """
    Convenience class that performs preprocessing for a given dataset.

    Parameters
    ----------
    data_factory : DataFactory
        Data factory containing the data set
    pre_processor_type : PreProcessorType
        Training or Testing
    create_plots : bool
        If True, plots are created and saved after certain steps
    """

    def __init__(self, data_factory: DataFactory, pre_processor_type: PreProcessorType,
                 create_plots: bool = False) -> None:
        info('Initializing PreProcessor...')
        self.data_factory = data_factory
        self.pre_processor_type = pre_processor_type
        self.create_plots = create_plots
        self.current_pipeline_step, self.df = data_factory.load_previous_data()

        if self.current_pipeline_step is PipelineStep.MERGE_RAW:
            self.df = data_factory.get_raw_data()
            self.current_pipeline_step = PipelineStep.get_next_step(self.current_pipeline_step)
        self.variable_summaries = None
        info('Plot Distributions...')
        if self.current_pipeline_step < PipelineStep.APPLY_IMPUTATIONS:
            self._plot_distributions(self.current_pipeline_step)
        elif not PipelineStep.EXTRACT_FEATURES <= self.current_pipeline_step:
            self._plot_distributions(self.current_pipeline_step, Variable.get_pre_processed_variables())

    def remove_erroneous_values(self) -> None:
        """
        Removes wrong values from data, i.e. unrealistic height and weight data.
        """
        step = PipelineStep.REMOVE_ERRORS
        if self.current_pipeline_step is step:
            info('Removing Erroneous Values...')
            self._remove_erroneous_values(Variable.Height, min_value=100, max_value=300)
            self._remove_erroneous_values(Variable.Weight, min_value=10)
            self.data_factory.save_pipeline_data(self.df, step)
            self.current_pipeline_step = PipelineStep.get_next_step(self.current_pipeline_step)
        else:
            warning(f'Please call the processing steps in increasing order ({self.current_pipeline_step})')

    def apply_transformations(self) -> None:
        """
        Applies transformations of troponin, creatinine, urine, and blood pressures.
        """
        step = PipelineStep.APPLY_TRANSFORMATIONS
        if self.current_pipeline_step is step:
            info('Applying transformations...')
            self._transform_troponin()
            self._transform_creatinine()
            self._transform_urine()
            self._transform_blood_pressure()
            self.data_factory.save_pipeline_data(self.df, step)
            self.current_pipeline_step = PipelineStep.get_next_step(self.current_pipeline_step)
        else:
            warning(f'Please call the processing steps in increasing order ({self.current_pipeline_step}')

    def get_variable_summaries(self) -> pd.DataFrame:
        """
        Returns the gender-specific mean and standard deviation values for each of the preprocessed variables.

        | Gender | (VariableOne, mean) | (VariableOne, std) | (VariableTwo, mean) | (VariableTwo, std) |
        | ------ | ------------------- | ------------------ | ------------------- | ------------------ |
        |    0.0 |          some_value |         some_value |          some_value |         some_value |
        |    1.0 |          some_value |         some_value |          some_value |         some_value |

        Returns
        -------
        pd.DataFrame
            Variable summaries
        """
        info('Getting variable summaries...')
        if self.variable_summaries:
            return self.variable_summaries

        gender = Variable.Gender.label

        variable_summaries = pd.DataFrame()
        genders = self.df[~self.df[gender].isna()][[ID_REPR, gender]]

        for variable in Variable.get_pre_processed_variables():
            if variable.imputation_method in [ImputationMethod.ONE, ImputationMethod.FOUR]:
                available_data = self.df[self.df[variable.label] > 0][[ID_REPR, variable.label]]
                available_data = available_data.merge(genders, on=[ID_REPR]).drop([ID_REPR], axis=1)
                variable_summary = available_data.groupby([gender]).aggregate(['mean', 'std'])
                variable_summaries = variable_summaries.merge(variable_summary, how='outer', left_index=True,
                                                              right_index=True)
        self.variable_summaries = variable_summaries
        return variable_summaries

    def impute_data(self, variable_summaries: pd.DataFrame = None) -> None:
        """
        Imputes data for all preprocessed variables.
        """
        step = PipelineStep.APPLY_IMPUTATIONS
        if self.current_pipeline_step is step:
            info('Applying imputations...')
            if self.pre_processor_type == PreProcessorType.TRAINING and variable_summaries is not None:
                raise AttributeError('The PreProcessor was initialized as type TRAINING, ' +
                                     'but during imputation other variable summaries were provided.')
            if self.pre_processor_type == PreProcessorType.TESTING and variable_summaries is None:
                raise AttributeError('The PreProcessor was initialized as type TESTING, ' +
                                     'but during imputation no other variable summaries were provided.')
            variables = Variable.get_pre_processed_variables()
            for variable in variables:
                if variable_summaries is not None:
                    self.df = _impute_data(self.df, variable, variable_summaries)
                else:
                    self.df = _impute_data(self.df, variable, self.variable_summaries)

            self._plot_distributions(step, variables)
            self.data_factory.save_pipeline_data(self.df, step)
            self.current_pipeline_step = PipelineStep.get_next_step(self.current_pipeline_step)
        else:
            warning(f'Please call the processing steps in increasing order ({self.current_pipeline_step}')

    def remove_outliers(self) -> None:
        """
        Removes outliers according to the outlier strategy present in the current data factory.
        """

        step = PipelineStep.REMOVE_OUTLIERS
        if self.current_pipeline_step is step:
            info('Removing outliers...')
            outlier_strategy = self.data_factory.outlier_strategy
            if outlier_strategy is not None:
                outliers = self.df.drop([ID_REPR, TIME_REPR], axis=1).apply(outlier_strategy.predict, axis=0)
                outlier_index = outliers[outliers.any(axis=1)].index
                self.df.drop(outlier_index, axis=0, inplace=True)

            self._plot_distributions(step, Variable.get_pre_processed_variables())
            self.data_factory.save_pipeline_data(self.df, step)
            self.current_pipeline_step = PipelineStep.get_next_step(self.current_pipeline_step)
        else:
            warning(f'Please call the processing steps in increasing order ({self.current_pipeline_step}')

    def extract_features(self) -> None:
        """
        Extracts features from the preprocessed variables according to their category.
        """
        step = PipelineStep.EXTRACT_FEATURES
        if self.current_pipeline_step is step:
            info('Extracting Features...')
            grouped = self.df.groupby([ID_REPR])
            self.df = pd.DataFrame()

            for category in VariableCategory:
                self.df = _extract_features_from_variable(self.df, grouped, category)

            self._add_blood_pressure_majority_features()
            self._add_outcomes()

            self.data_factory.save_pipeline_data(self.df, step)
            self.current_pipeline_step = PipelineStep.get_next_step(self.current_pipeline_step)
        else:
            warning(f'Please call the processing steps in increasing order ({self.current_pipeline_step}')

    def _add_outcomes(self):
        outcomes = self.data_factory.get_outcome_file()
        outcomes.drop(IRRELEVANT_OUTCOME_DATA, axis=1, inplace=True)
        self.df = self.df.merge(outcomes, left_on=[ID_REPR], right_on=[RECORD_ID_REPR], how='outer')
        self.df.drop([RECORD_ID_REPR], axis=1, inplace=True)

    def _add_blood_pressure_majority_features(self):
        for blood_pressure in [Variable.DABP, Variable.SABP, Variable.MABP]:
            binary_majority_feature = self.data_factory.get_file(blood_pressure.full_name)
            self.df = self.df.merge(binary_majority_feature, on=[ID_REPR], how='outer')
        self.df.fillna(0.0, inplace=True)

    def _plot_distributions(self, step: PipelineStep, variables: List[Variable] = None) -> None:
        """
        Creates a new plot factory with the current data and plots the given variables. Also saves plots to disk.
        """
        if self.create_plots:
            PlotFactory(self.df).plot_distributions(variables=variables, save_plots=True, attribute=step.label)

    def _remove_erroneous_values(self, variable: Variable, **args: Union[float, bool]) -> None:
        """
        Removes erroneous values for the given variable. Additional parameters are:

        min_value : float
            If present, transformed data will fulfill value >= min_value
        max_value : float
            If present, transformed data will fulfill value <= max_value
        retain_zeros : bool
            If present, transformed data will retain values of zeros, this trumps the previous requirements
        retain_nans : bool
            If present, transformed data will retain nan values, this trumps the previous requirements

        Parameters
        ----------
        variable : Variable
            Variable to be transformed
        """
        info(f'... for {variable.full_name}')
        before = 0
        after = 0
        before += self.df.shape[0]
        self.df = _remove_erroneous_values_from_df(self.df, variable, **args)
        after += self.df.shape[0]
        info(f'removed {before - after} samples')
        self._plot_distributions(self.current_pipeline_step, [variable])

    def _transform_troponin(self) -> None:
        """
        Merges TroponinI and TroponinT to gain Trponin by multiplying TroponinT with 100. If both are present, the mean
        is chosen.
        """
        info(f'... for {Variable.Troponin.full_name}')
        self.df = _transform_troponin(self.df)

    def _transform_creatinine(self) -> None:
        """
        Transforms Creatinine to CreatinineClearance according to:

        CreatinineClearance := (140 - Age) * Weight * (0.85 + 0.15 * Gender) / (72 * Creatinine)
        """
        info(f'... for {Variable.Creatinine.full_name}')
        self.df = _transform_creatinine(self.df)

    def _transform_urine(self) -> None:
        """
        Replaces urine with urine sum.
        """
        info(f'... for {Variable.Urine.full_name}')
        self.df = _transform_urine(self.df)

    def _transform_blood_pressure(self) -> None:
        """
        Merges invasive and non invasive measurements for diastolic, systolic, and mean arterial bloop pressure.
        Additionally, for each blood pressure, creates a binary variable for each case, containing whether or not the
        majority of measurements were taken non invasive. Those binary variables are stored to disk.
        """
        self.df, dabp = _transform_by_joining_and_creating_binary_majority_variable(self.df, Variable.DiasABP,
                                                                                    Variable.NIDiasABP, Variable.DABP)
        self.df, sabp = _transform_by_joining_and_creating_binary_majority_variable(self.df, Variable.SysABP,
                                                                                    Variable.NISysABP, Variable.SABP)
        self.df, mabp = _transform_by_joining_and_creating_binary_majority_variable(self.df, Variable.MAP,
                                                                                    Variable.NIMAP, Variable.MABP)
        self.data_factory.save_to_file(dabp, Variable.DABP.full_name)
        self.data_factory.save_to_file(sabp, Variable.SABP.full_name)
        self.data_factory.save_to_file(mabp, Variable.MABP.full_name)


def _remove_erroneous_values_from_df(df: pd.DataFrame, variable: Variable, min_value: float = None,
                                     max_value: float = None,
                                     retain_zeros: bool = True, retain_nans: bool = True) -> pd.DataFrame:
    """
    Depending on the given values, removes all data that does not fulfill the requirements.

    Parameters
    ----------
    df : pd.DataFrame
        Data frame to be transformed
    variable : Variable
        Variable to be transformed
    min_value : float
        If present, transformed data will fulfill value >= min_value
    max_value : float
        If present, transformed data will fulfill value <= max_value
    retain_zeros : bool
        If present, transformed data will retain values of zeros, this trumps the previous requirements
    retain_nans : bool
        If present, transformed data will retain nan values, this trumps the previous requirements

    Returns
    -------
    pd.DataFrame
        Transformed data frame
    """
    c_1 = c_2 = df[variable.label]
    if max_value is not None:
        c_1 = df[variable.label] <= max_value
    if min_value is not None:
        c_2 = df[variable.label] >= min_value
    conditions = conjunction(c_1, c_2)
    if retain_zeros:
        c_3 = df[variable.label] <= 0
        conditions = disjunction(conditions, c_3)
    if retain_nans:
        c_4 = df[variable.label].isna()
        conditions = disjunction(conditions, c_4)
    return df[conditions]


def _transform_troponin(df: pd.DataFrame) -> pd.DataFrame:
    """
    Merges TroponinI and TroponinT to gain Troponin by multiplying TroponinT with 100. If both are present, the mean
    is chosen.
    """
    df.loc[:, Variable.Troponin.label] = pd.concat(
        [df[Variable.TroponinT.label] * 100, df[Variable.TroponinI.label]], axis=1).mean(axis=1)
    df.drop([Variable.TroponinT.label, Variable.TroponinI.label], axis=1, inplace=True)
    return df


def _transform_creatinine(df: pd.DataFrame) -> pd.DataFrame:
    """
    Transforms Creatinine to CreatinineClearance according to:

    CreatinineClearance := (140 - Age) * Weight * (0.85 + 0.15 * Gender) / (72 * Creatinine)

    Parameters
    ----------
    df : pd.DataFrame
        Data frame with the data

    Returns
    -------
    pd.DataFrame
        Updated data frame
    """

    # Convenience Variables to make code better readable
    gender = Variable.Gender.label
    age = Variable.Age.label
    weight = Variable.Weight.label
    creatinine = Variable.Creatinine.label
    creatinine_clearance = Variable.CreatinineClearance.label

    genders = df[~df[gender].isna()][[ID_REPR, gender]]
    ages = df[~df[age].isna()][[ID_REPR, age]]
    weights = df[~df[weight].isna()][[ID_REPR, weight, TIME_REPR]]
    creatinines = df[~df[creatinine].isna()][[ID_REPR, creatinine, TIME_REPR]]
    weights = weights.merge(genders, on=[ID_REPR])
    mean_weights = weights.drop([ID_REPR], axis=1).groupby([gender]).describe()[(weight, 'mean')]

    def get_most_recent_weight(patient_id, time, gender_value):
        c_1 = weights[ID_REPR] == patient_id
        c_2 = weights[TIME_REPR] < time
        try:
            return weights[conjunction(c_1, c_2)].sort_values(TIME_REPR, ascending=False).iloc[0][weight]
        except IndexError:
            return mean_weights[gender_value]

    creatinines = creatinines.reset_index()
    creatinines = creatinines.merge(genders, on=[ID_REPR])
    creatinines = creatinines.merge(ages, on=[ID_REPR])
    creatinines.loc[:, weight] = creatinines.apply(
        lambda x: get_most_recent_weight(x[ID_REPR], x[TIME_REPR], x[gender]), axis=1)
    creatinines.loc[:, creatinine_clearance] = ((140 - creatinines[age]) * creatinines[weight] * (
            0.85 + 0.15 * creatinines[gender])) / (72 * creatinines[creatinine])
    creatinines = creatinines.set_index('index')
    df.loc[creatinines.index, creatinine_clearance] = creatinines[creatinine_clearance]
    df.drop([creatinine], axis=1, inplace=True)
    return df


def _transform_urine(df: pd.DataFrame) -> pd.DataFrame:
    """
    Replaces urine with urine sum.

    Parameters
    ----------
    df : pd.DataFrame
        Data frame with the data

    Returns
    -------
    pd.DataFrame
        Updated data frame
    """
    urines = df[~df[Variable.Urine.label].isna()][[ID_REPR, Variable.Urine.label, TIME_REPR]]
    urines[Variable.UrineSum.label] = urines.groupby([ID_REPR])[Variable.Urine.label].cumsum()
    df.loc[urines.index, Variable.UrineSum.label] = urines[Variable.UrineSum.label]
    df.drop([Variable.Urine.label], axis=1, inplace=True)
    return df


def _transform_by_joining_and_creating_binary_majority_variable(df: pd.DataFrame,
                                                                v1: Variable,
                                                                v2: Variable,
                                                                new_v: Variable) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Merges the invasive and non invasive measurements of a variable and creates a new binary variable that contains
    information whether or not the majority of measurements were taken non invasive.

    Parameters
    ----------
    df : pd.DataFrame
        Data frame with the data
    v1 : Variable
        First variable to be joined
    v2 : Variable
        Second variable to be joined
    new_v : Variable
        Variable representation of the joined variables

    Returns
    -------
    Tuple[pd.DataFrame, pd.DataFrame]
        The adjusted Data frame and the new binary variable
    """
    info(f'... for {new_v.full_name}')
    invasives = df[~df[v1.label].isna()][[ID_REPR, v1.label, TIME_REPR]]
    non_invasives = df[~df[v2.label].isna()][[ID_REPR, v2.label, TIME_REPR]]
    invasives.rename(columns={v1.label: new_v.label}, inplace=True)
    non_invasives.rename(columns={v2.label: new_v.label}, inplace=True)

    # merge invasive and non-invasive diastolic blood pressure
    joined_variables = pd.concat([invasives, non_invasives])
    df.loc[joined_variables.index.values, new_v.label] = joined_variables[new_v.label]
    df.drop([v1.label, v2.label], axis=1, inplace=True)

    # create new variable with majority of invasive or non-invasive readings
    counts = invasives.set_index([ID_REPR, TIME_REPR]).count(level=ID_REPR)
    ni_counts = non_invasives.set_index([ID_REPR, TIME_REPR]).count(level=ID_REPR)

    new_counts = counts.reset_index()
    new_counts = new_counts.merge(ni_counts, on=[ID_REPR])
    new_counts.columns = [ID_REPR, 'v1_count', 'v2_count']
    new_counts[new_v.label + '_maj_invasive'] = (new_counts['v1_count'] < new_counts['v2_count']).astype('float64')
    new_counts.drop(['v1_count', 'v2_count'], axis=1, inplace=True)
    return df, new_counts


def _impute_data(df: pd.DataFrame, variable: Variable, variable_summaries: pd.DataFrame) -> pd.DataFrame:
    """
    Imputes data in the given data frame for the given variable.

    Parameters
    ----------
    df : pd.DataFrame
        Data with missing or absent values
    variable : Variable
        Variable for which data is imputed, dictates the imputation method
    variable_summaries : pd.DataFrame
        Summary data for all variables in df used for data imputation

    Returns
    -------
    pd.DataFrame
        Original data with replaced missing and added absent values
    """
    info(f'... for {variable.full_name}')

    genders = df[~df[Variable.Gender.label].isna()][[ID_REPR, Variable.Gender.label]]

    if variable.imputation_method is ImputationMethod.ONE:
        mean_values = variable_summaries[(variable.label, 'mean')]

        def sample_function(gender):
            return mean_values[gender]
    elif variable.imputation_method is ImputationMethod.FOUR:
        def sample_function(gender):
            mean = variable_summaries.at[gender, (variable.label, 'mean')]
            std = variable_summaries.at[gender, (variable.label, 'std')]
            return np.random.normal(mean, std)
    else:
        warning(f'No data was imputed for {variable.full_name}, as the chosen method '
                f'"{variable.imputation_method}" is unknown.')
        return df

    df = _impute_empty_measurements(df, variable, genders, sample_function)
    df = _impute_absent_measurements(df, variable, genders, sample_function)
    return df


def _impute_absent_measurements(df: pd.DataFrame, variable: Variable, genders: pd.DataFrame,
                                sample_function: Callable[[float], float]) -> pd.DataFrame:
    """
    Adds missing values, i.e., if a case does not have any measurement for any considered variable, it is added at
    'default time' (the earliest possible time, in order to be considered for all icu stays.
    """
    grouped = df.groupby([ID_REPR])
    new_values = []
    for case_id, group in grouped:
        if group[variable.label].count() < 1:
            # no data is given for variable, i.e. data is missing, but not indicated as such
            gender = genders[genders[ID_REPR] == case_id][Variable.Gender.label].values[0]
            new_values.append({ID_REPR: case_id, variable.label: sample_function(gender), TIME_REPR: DEFAULT_TIME})
    df = df.append(pd.DataFrame(new_values), ignore_index=True)
    return df


def _impute_empty_measurements(df: pd.DataFrame, variable: Variable, genders: pd.DataFrame,
                               sample_function: Callable[[float], float]) -> pd.DataFrame:
    """
    Replaces empty values, i.e., if a measurement is present, but its value indicates missingness (usually zero or
    below).
    """
    gender = Variable.Gender.label
    missing_data = df[df[variable.label] <= 0][[ID_REPR, variable.label, TIME_REPR]]
    if not missing_data.empty:
        index = missing_data.index
        missing_data = missing_data.merge(genders, on=[ID_REPR])
        missing_data.index = index
        try:
            missing_data[variable.label] = missing_data.apply(lambda x: sample_function(x[gender]), axis=1)

            # remove gender data to keep one measurement per entry
            missing_data[gender] = np.nan
            df.loc[missing_data.index.values] = missing_data
        except ValueError:
            info(f'No missing data for {variable.full_name}')
        return df
    return df


def _extract_features_from_variable(df: pd.DataFrame, grouped: pd.DataFrame, variable_category: VariableCategory, ):
    for variable in Variable.get_by_values([variable_category]):
        if variable in Variable.get_pre_processed_variables():
            info(f'... for {variable.label}')
            df = variable.extract_features(df, grouped)
    return df
