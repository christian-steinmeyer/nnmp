from enum import Enum
from logging import warning
from typing import NamedTuple, List, Union, Iterable, Set

import numpy as np
import pandas as pd

from constants import TIME_REPR, ID_REPR
from physionetChallenge.src.ImputationMethod import ImputationMethod
from physionetChallenge.src.VariableCategory import VariableCategory


class NormalRange(NamedTuple):
    """
    Representation of a normal range, i.e., given a gender, a lower and upper bound that define the normal range.
    """
    gender: float
    lower_bound: float
    upper_bound: float


class Variable(Enum):
    """
    Representation of all variables, either available in the original PhysioNet Challenge Dataset, or generated as part
    of the preprocessing pipeline.

    Parameters
    ----------
    full_name : str
        Full name of the variable (possibly containing an abbreviation in parentheses)
    variable_category : VariableCategory
        Category of the variable
    imputation_method : ImputationMethod
        Imputation method used for this variable
    female_normal_range : NormalRange
        Normal range for females
    male_normal_range : NormalRange
        Normal range for males
    """
    # original features
    ALP = ('Alkaline Phosphatase(ALP)',
           'ALP',
           VariableCategory.LOW_SAMPLING_VARIABLE,
           None,
           NormalRange(0.0, 44, 147),
           NormalRange(1.0, 44, 147))
    ALT = ('Alanine Transaminase (ALT)',
           'ALT',
           VariableCategory.LOW_SAMPLING_VARIABLE,
           None,
           NormalRange(0.0, 10, 50),
           NormalRange(1.0, 5, 38))
    AST = ('Aspartate Transaminase (AST)',
           'AST',
           VariableCategory.LOW_SAMPLING_VARIABLE,
           None,
           NormalRange(0.0, 8, 40),
           NormalRange(1.0, 6, 34))
    Age = ('Age',
           'Age',
           VariableCategory.GENERAL_DESCRIPTOR,
           None,
           None,
           None)
    Albumin = ('Albumin',
               'Albumin',
               VariableCategory.LOW_SAMPLING_VARIABLE,
               None,
               NormalRange(0.0, 3.5, 5.4),
               NormalRange(1.0, 3.5, 5.4))
    BUN = ('Blood Urea Nitrogen (BUN)',
           'BUN',
           VariableCategory.MED_SAMPLING_VARIABLE,
           ImputationMethod.FOUR,
           NormalRange(0.0, 6, 20),
           NormalRange(1.0, 6, 20))
    Bilirubin = ('Bilirubin',
                 'Bilirubin',
                 VariableCategory.LOW_SAMPLING_VARIABLE,
                 None,
                 NormalRange(0.0, 0.2, 1.9),
                 NormalRange(1.0, 0.2, 1.9))
    Cholesterol = ('Cholesterol',
                   'Cholesterol',
                   VariableCategory.LOW_SAMPLING_VARIABLE,
                   None,
                   NormalRange(0.0, 200, 1000),
                   NormalRange(1.0, 200, 1000))
    Creatinine = ('Serum Creatinine (Creatinine)',
                  'Creatinine',
                  VariableCategory.MED_SAMPLING_VARIABLE,
                  ImputationMethod.REPLACED,
                  NormalRange(0.0, 0.6, 1.1),
                  NormalRange(1.0, 0.7, 1.3))
    DiasABP = ('Invasive Diastolic Arterial Blood Pressure (DiasABP)',
               'DiasABP',
               VariableCategory.HIGH_SAMPLING_VARIABLE,
               ImputationMethod.REPLACED,
               NormalRange(0.0, 60, 90),
               NormalRange(1.0, 60, 90))
    FiO2 = ('Fractional inspired Oxygen (FiO2)',
            'FiO2',
            VariableCategory.HIGH_SAMPLING_VARIABLE,
            ImputationMethod.FOUR,
            NormalRange(0.0, 0.21, 0.5),
            NormalRange(1.0, 0.21, 0.5))
    GCS = ('Glasgow Coma Score (GCS)',
           'GCS',
           VariableCategory.HIGH_SAMPLING_VARIABLE,
           ImputationMethod.FOUR,
           NormalRange(0.0, 0, 3),
           NormalRange(1.0, 0, 3))
    Gender = ('Gender',
              'Gender',
              VariableCategory.GENERAL_DESCRIPTOR,
              None,
              None,
              None)
    Glucose = ('Serum Glucose (Glucose)',
               'Glucose',
               VariableCategory.MED_SAMPLING_VARIABLE,
               ImputationMethod.FOUR,
               NormalRange(0.0, 70, 100),
               NormalRange(1.0, 70, 100))
    HCO3 = ('Serum Bicarbonate (HCO3)',
            'HCO3',
            VariableCategory.MED_SAMPLING_VARIABLE,
            ImputationMethod.FOUR,
            NormalRange(0.0, 23, 29),
            NormalRange(1.0, 23, 29))
    HCT = ('Hematocrit (HCT)',
           'HCT',
           VariableCategory.MED_SAMPLING_VARIABLE,
           ImputationMethod.FOUR,
           NormalRange(0.0, 35, 48),
           NormalRange(1.0, 40, 53))
    HR = ('Heart Rate (HR)',
          'HR',
          VariableCategory.HIGH_SAMPLING_VARIABLE,
          ImputationMethod.FOUR,
          NormalRange(0.0, 60, 100),
          NormalRange(1.0, 60, 100))
    Height = ('Height',
              'Height',
              VariableCategory.GENERAL_DESCRIPTOR,
              ImputationMethod.ONE,
              None,
              None)
    ICUType = ('Intensive Care Unit Type (ICUType)',
               'ICUType',
               VariableCategory.GENERAL_DESCRIPTOR,
               None,
               None,
               None)
    K = ('Serum Potassium (K)',
         'K',
         VariableCategory.MED_SAMPLING_VARIABLE,
         ImputationMethod.FOUR,
         NormalRange(0.0, 0.5, 2.2),
         NormalRange(1.0, 0.5, 2.2))
    Lactate = ('Lactate',
               'Lactate',
               VariableCategory.MED_SAMPLING_VARIABLE,
               ImputationMethod.FOUR,
               NormalRange(0.0, 3.7, 5.2),
               NormalRange(1.0, 3.7, 5.2))
    MAP = ('Invasive Mean Arterial Pressure (MAP)',
           'MAP',
           VariableCategory.HIGH_SAMPLING_VARIABLE,
           ImputationMethod.REPLACED,
           NormalRange(0.0, 70, 100),
           NormalRange(1.0, 70, 100))
    MechVent = ('Mechanical Ventilation Respiration (MechVent)',
                'MechVent',
                VariableCategory.GENERAL_DESCRIPTOR,
                None,
                None,
                None)
    Mg = ('Serum Magnesium (Mg)',
          'Mg',
          VariableCategory.MED_SAMPLING_VARIABLE,
          ImputationMethod.FOUR,
          NormalRange(0.0, 1.7, 2.2),
          NormalRange(1.0, 1.7, 2.2))
    NIDiasABP = ('Non-invasive Diastolic Arterial Blood Pressure (NIDiasABP)',
                 'NIDiasABP',
                 VariableCategory.HIGH_SAMPLING_VARIABLE,
                 ImputationMethod.REPLACED,
                 NormalRange(0.0, 60, 90),
                 NormalRange(1.0, 60, 90))
    NIMAP = ('Non-invasive Mean Arterial Pressure (NIMAP)',
             'NIMAP',
             VariableCategory.HIGH_SAMPLING_VARIABLE,
             ImputationMethod.REPLACED,
             NormalRange(0.0, 70, 100),
             NormalRange(1.0, 70, 100))
    NISysABP = ('Non-invasive Systolic Arterial Blood Pressure (NISysABP)',
                'NISysABP',
                VariableCategory.HIGH_SAMPLING_VARIABLE,
                ImputationMethod.REPLACED,
                NormalRange(0.0, 100, 140),
                NormalRange(1.0, 100, 140))
    Na = ('Sodium (Na)',
          'Na',
          VariableCategory.MED_SAMPLING_VARIABLE,
          ImputationMethod.FOUR,
          NormalRange(0.0, 135, 145),
          NormalRange(1.0, 135, 145))
    PaCO2 = ('Partial Pressure of Arterial Carbon Dioxide (PaCO2)',
             'PaCO2',
             VariableCategory.MED_SAMPLING_VARIABLE,
             ImputationMethod.FOUR,
             NormalRange(0.0, 35, 45),
             NormalRange(1.0, 35, 45))
    PaO2 = ('Partial Pressure of Arterial Oxygen (PaO2)',
            'PaO2',
            VariableCategory.MED_SAMPLING_VARIABLE,
            ImputationMethod.FOUR,
            NormalRange(0.0, 75, 100),
            NormalRange(1.0, 75, 100))
    Platelets = ('Platelets',
                 'Platelets',
                 VariableCategory.MED_SAMPLING_VARIABLE,
                 ImputationMethod.FOUR,
                 NormalRange(0.0, 150, 450),
                 NormalRange(1.0, 150, 450))
    RespRate = ('Respiratory Rate (RespRate)',
                'RespRate',
                VariableCategory.LOW_SAMPLING_VARIABLE,
                None,
                NormalRange(0.0, 10, 20),
                NormalRange(1.0, 10, 20))
    SaO2 = ('Oxygen Saturation in Hemoglobin (SaO2)',
            'SaO2',
            VariableCategory.LOW_SAMPLING_VARIABLE,
            None,
            NormalRange(0.0, 94, 100),
            NormalRange(1.0, 94, 100))
    SysABP = ('Invasive Systolic Arterial Blood Pressure (SysABP)',
              'SysABP',
              VariableCategory.HIGH_SAMPLING_VARIABLE,
              ImputationMethod.REPLACED,
              NormalRange(0.0, 100, 140),
              NormalRange(1.0, 100, 140))
    Temp = ('Temperature (Temp)',
            'Temp',
            VariableCategory.HIGH_SAMPLING_VARIABLE,
            ImputationMethod.FOUR,
            NormalRange(0.0, 36, 40),
            NormalRange(1.0, 36, 40))
    TroponinI = ('TroponinI',
                 'TroponinI',
                 VariableCategory.LOW_SAMPLING_VARIABLE,
                 ImputationMethod.REPLACED,
                 NormalRange(0.0, 0, 10),
                 NormalRange(1.0, 0, 10))
    TroponinT = ('TroponinT',
                 'TroponinT',
                 VariableCategory.LOW_SAMPLING_VARIABLE,
                 ImputationMethod.REPLACED,
                 NormalRange(0.0, 0, 0.1),
                 NormalRange(1.0, 0, 0.1))
    Urine = ('Urine',
             'Urine',
             VariableCategory.HIGH_SAMPLING_VARIABLE,
             ImputationMethod.REPLACED,
             None,
             None)
    WBC = ('White Blood Cell Count (WBC)',
           'WBC',
           VariableCategory.MED_SAMPLING_VARIABLE,
           ImputationMethod.FOUR,
           NormalRange(0.0, 4.5, 10),
           NormalRange(1.0, 4.5, 10))
    Weight = ('Weight',
              'Weight',
              VariableCategory.HIGH_SAMPLING_VARIABLE,
              ImputationMethod.ONE,
              None,
              None)
    pH = ('Arterial pH (pH)',
          'pH',
          VariableCategory.MED_SAMPLING_VARIABLE,
          ImputationMethod.FOUR,
          NormalRange(0.0, 7.38, 7.42),
          NormalRange(1.0, 7.38, 7.42))

    # new features
    CreatinineClearance = ('Creatinine Clearance',
                           'CreatinineClearance',
                           VariableCategory.MED_SAMPLING_VARIABLE,
                           ImputationMethod.FOUR,
                           NormalRange(0.0, 88, 128),
                           NormalRange(1.0, 97, 137))
    Troponin = ('Troponin',
                'Troponin',
                VariableCategory.LOW_SAMPLING_VARIABLE,
                None,
                NormalRange(0.0, 0, 10),
                NormalRange(1.0, 0, 10))
    UrineSum = ('Urine Sum',
                'Urine.Sum',
                VariableCategory.HIGH_SAMPLING_VARIABLE,
                ImputationMethod.FOUR,
                None,
                None)
    DABP = ('Diastolic Arterial Blood Pressure (DABP)',
            'DABP',
            VariableCategory.HIGH_SAMPLING_VARIABLE,
            ImputationMethod.FOUR,
            NormalRange(0.0, 60, 90),
            NormalRange(1.0, 60, 90))
    SABP = ('Systolic Arterial Blood Pressure (SABP)',
            'SABP',
            VariableCategory.HIGH_SAMPLING_VARIABLE,
            ImputationMethod.FOUR,
            NormalRange(0.0, 100, 140),
            NormalRange(1.0, 100, 140))
    MABP = ('Mean Arterial Blood Pressure (MABP)',
            'MABP',
            VariableCategory.HIGH_SAMPLING_VARIABLE,
            ImputationMethod.FOUR,
            NormalRange(0.0, 70, 100),
            NormalRange(1.0, 70, 100))

    def __init__(self, full_name: str, label: str, variable_category: VariableCategory,
                 imputation_method: ImputationMethod, female_normal_range: NormalRange,
                 male_normal_range: NormalRange) -> None:
        self.full_name = full_name
        self.label = label
        self.variable_category = variable_category
        self.imputation_method = imputation_method
        self.female_normal_range = female_normal_range
        self.male_normal_range = male_normal_range

    def within_normal_range(self, gender: float, value: float):
        """
        Returns whether or not a given value is within the normal range for the given sex.

        Parameters
        ----------
        gender : float
            either 0.0 for female or 1.0 for male
        value : float
            value to be evaluated

        Returns
        -------
        bool
            True if value in normal range, False otherwise
        """
        if gender == -1.0:
            # out of the ordinary case with little available information, treat as not normal
            return False

        normal_range = None
        if gender == 0.0:
            normal_range = self.female_normal_range
        elif gender == 1.0:
            normal_range = self.male_normal_range

        if not normal_range:
            raise RuntimeWarning('Cannot check bounds for incomplete feature', self)

        return normal_range.lower_bound <= value <= normal_range.upper_bound

    def has_value(self, value) -> bool:
        """
        For a given argument or given value, returns, whether the current Variable contains that value.
        """
        return {value}.issubset(set(self.value))

    def extract_features(self, df: pd.DataFrame, grouped: pd.DataFrame) -> pd.DataFrame:
        if self.variable_category is VariableCategory.GENERAL_DESCRIPTOR:
            return self._get_max_values_from_variable(df, grouped)
        if self.variable_category is VariableCategory.LOW_SAMPLING_VARIABLE:
            return self._get_normal_range_relation_for_variable(df, grouped)
        if self.variable_category is VariableCategory.MED_SAMPLING_VARIABLE:
            return self._get_mean_for_variable(df, grouped)
        if self.variable_category is VariableCategory.HIGH_SAMPLING_VARIABLE:
            return self._get_summary_statistics_for_variable(df, grouped)
        raise RuntimeError(f'Unexpected variable category encountered: {self.variable_category}. '
                           f'Cannot extract features.')

    @classmethod
    def get_by_values(cls, values: Iterable[Union[ImputationMethod, VariableCategory]]) -> Set['Variable']:
        """
        Convenience method to retrieve all variables of the given criterion.

        Parameters
        ----------
        values : Iterable[Union[ImputationMethod, VariableCategory]]
            values to be considered, either VariableCategory or ImputationMethod

        Returns
        -------
        List[Variable]
            All variables that contain exact value
        """
        return {member for member in cls if any({member.has_value(value) for value in values})}

    @classmethod
    def get_original_variables(cls) -> List['Variable']:
        """
        Convenience method to retrieve all variables that were given in the PhysioNet Challenge dataset.

        Returns
        -------
        List[Variable]
            All variables originally provided
        """
        return [cls.ALP, cls.ALT, cls.AST, cls.Age, cls.Albumin, cls.BUN, cls.Bilirubin, cls.Cholesterol,
                cls.Creatinine, cls.DiasABP, cls.FiO2, cls.GCS, cls.Gender, cls.Glucose, cls.HCO3, cls.HCT,
                cls.HR, cls.Height, cls.ICUType, cls.K, cls.Lactate, cls.MAP, cls.MechVent, cls.Mg,
                cls.NIDiasABP, cls.NIMAP, cls.NISysABP, cls.Na, cls.PaCO2, cls.PaO2, cls.Platelets,
                cls.RespRate, cls.SaO2, cls.SysABP, cls.Temp, cls.TroponinI, cls.TroponinT, cls.Urine, cls.WBC,
                cls.Weight, cls.pH]

    @classmethod
    def get_pre_processed_variables(cls) -> List['Variable']:
        """
        Convenience method to retrieve all variables that are retained or created during preprocessing.

        Returns
        -------
        List[Variable]
            All variables that are preprocessed
        """
        return [cls.ALP, cls.ALT, cls.AST, cls.Age, cls.Albumin, cls.BUN, cls.Bilirubin, cls.Cholesterol,
                cls.CreatinineClearance, cls.FiO2, cls.GCS, cls.Gender, cls.Glucose, cls.HCO3, cls.HCT, cls.HR,
                cls.Height, cls.ICUType, cls.K, cls.Lactate, cls.MechVent, cls.Mg, cls.Na, cls.PaCO2, cls.PaO2,
                cls.Platelets, cls.RespRate, cls.SaO2, cls.Temp, cls.Troponin, cls.UrineSum, cls.WBC,
                cls.Weight, cls.pH, cls.DABP, cls.SABP, cls.MABP]

    def _get_max_values_from_variable(self, df: pd.DataFrame, grouped: pd.DataFrame) -> pd.DataFrame:
        """
            Adds general descriptor features from a variable data frame grouped by case id to a feature data frame. General
            descriptors are added as are.
    
            Parameters
            ----------
            df : pd.DataFrame
                Data frame containing feature data (general descriptors are added to this data frame)
            grouped : pd.DataFrame
                Data frame containing variable data grouped by case id (general descriptors are taken from this data frame)
    
            Returns
            -------
            pd.DataFrame
                Extended data frame containing features
            """
        df[self.label] = grouped[self.label].max()
        if self is Variable.MechVent:
            df.fillna(value={Variable.MechVent.label: 0.0}, inplace=True)
        return df

    def _determine_normal_range_relation(self, gender: float, group: pd.DataFrame):
        number_of_measurements = group[self.label].count()
        if number_of_measurements < 1:
            return 0
        number_of_normal_measurements = group.apply(lambda row: self.within_normal_range(gender, row[self.label]),
                                                    axis=1).sum()
        if number_of_measurements == number_of_normal_measurements:
            return 1
        elif number_of_measurements > number_of_normal_measurements:
            return 2
        else:
            warning(f'Tried to determine the normal range for {self.full_name}, but the number of normal measurements '
                    f'was greater than the number of measurements.')

    def _get_normal_range_relation_for_variable(self, df: pd.DataFrame, grouped: pd.DataFrame) -> pd.DataFrame:
        """
        Adds low sampling features from a variable data frame grouped by case id to a feature data frame. Low sampling
        variables are added as either 0, if not measured at all, as 1, if all measurements were normal, or as 2, as soon as
        at least one of the measurements was abnormal.
    
        Parameters
        ----------
        df : pd.DataFrame
            Data frame containing feature data (low sampling features are added to this data frame)
        grouped : pd.DataFrame
            Data frame containing variable data grouped by case id (low sampling features are taken from this data frame)
    
        Returns
        -------
        pd.DataFrame
            Extended data frame containing features
        """
        gender = Variable.Gender.label
        index = df.index
        values = df.reset_index().apply(
            lambda row: self._determine_normal_range_relation(row[gender], grouped.get_group(row[ID_REPR])),
            axis=1)
        values.index = index
        df[self.label] = values
        return df

    def _linear_trends(self, df: pd.DataFrame) -> np.array:
        df.dropna(subset=[self.label], inplace=True)
        one_day = pd.to_timedelta('24:00:00')
        day_one = df[df[TIME_REPR] < one_day]
        day_two = df[df[TIME_REPR] >= one_day]

        result = np.array([0.0, 0.0, 0.0])

        if df.shape[0] > 9:
            result[0] = np.polyfit(df[TIME_REPR].dt.total_seconds(), df[self.label], 1)[0]
        if day_one.shape[0] > 4:
            result[1] = np.polyfit(day_one[TIME_REPR].dt.total_seconds(), day_one[self.label], 1)[0]
        if day_two.shape[0] > 4:
            result[2] = np.polyfit(day_two[TIME_REPR].dt.total_seconds(), day_two[self.label], 1)[0]

        return result

    def _get_mean_for_variable(self, df: pd.DataFrame, grouped: pd.DataFrame) -> pd.DataFrame:
        """
        Adds low sampling features from a variable data frame grouped by case id to a feature data frame. Med sampling
        variables are added as either the mean of all available measurements, if at least one measurement was taken, or
        negative one, if no measurement was taken.
    
    
        Parameters
        ----------
        df : pd.DataFrame
            Data frame containing feature data (low sampling features are added to this data frame)
        grouped : pd.DataFrame
            Data frame containing variable data grouped by case id (low sampling features are taken from this data frame)
    
        Returns
        -------
        pd.DataFrame
            Extended data frame containing features
        """
        df[self.label] = grouped[self.label].agg('mean')
        return df

    def _get_summary_statistics_for_variable(self, df: pd.DataFrame, grouped: pd.DataFrame) -> pd.DataFrame:
        """
        Adds low sampling features from a variable data frame grouped by case id to a feature data frame. Low sampling
        variables are added as either 0, if not measured at all, as 1, if all measurements were normal, or as 2, as soon as
        at least one of the measurements was abnormal.
    
        Parameters
        ----------
        df : pd.DataFrame
            Data frame containing feature data (low sampling features are added to this data frame)
        grouped : pd.DataFrame
            Data frame containing variable data grouped by case id (low sampling features are taken from this data frame)
    
        Returns
        -------
        pd.DataFrame
            Extended data frame containing features
        """
        aggregations = ['min', 'mean', 'max', 'median', 'first', 'last']
        columns = [f'{self.label}_{x}' for x in aggregations]
        result = grouped[self.label].agg(aggregations)
        result.columns = columns
        trends = df.reset_index().apply(
            lambda row: self._linear_trends(grouped.get_group(row['ID']).copy()),
            axis=1)
        result[f'{self.label}_trend_two_days'], result[f'{self.label}_trend_day_one'], result[
            f'{self.label}_trend_day_two'] = zip(*trends)
        df = df.merge(result, on=['ID'], how='outer')
        return df
