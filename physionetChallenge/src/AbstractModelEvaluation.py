import time
from abc import ABC, abstractmethod
from typing import Mapping, Any

from imblearn.base import BaseSampler
from keras.utils import to_categorical
from numpy.random import RandomState
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.feature_selection import chi2, SelectKBest
from sklearn.preprocessing import MinMaxScaler

from physionetChallenge.src.DataFactory import *
from physionetChallenge.src.EnsembleClassifier import EnsembleClassifier
from src.utils import *


class AbstractModelEvaluation(ABC):
    """
    Convenience class that can perform a model evaluation on preprocessed data.
    """

    def __init__(self, number_of_features: int = None, scaler: (BaseEstimator, TransformerMixin) = MinMaxScaler(),
                 ensemble_size: int = 7, parameter_map: ParameterMap = None, data_factory: DataFactory = None):
        if not number_of_features:
            raise AttributeError(f'Cannot create model evaluation object without number of features.')
        if not parameter_map:
            raise AttributeError(f'Cannot create grid search object without parameters.')
        if not data_factory:
            raise AttributeError(f'Cannot create model evaluation object without data factory.')
        self.number_of_features = number_of_features
        self.random_state = RandomState(42)
        self.scaler = scaler
        self.ensemble_size = ensemble_size
        self.parameter_map = parameter_map
        self.data_factory = data_factory

        # need setting through set_data
        self.data = None
        self.target = None
        self.X = None
        self.y = None
        self.X_test = None
        self.y_test = None

        # needs setting through add_balance_strategy

    def set_data(self, target_variable_name: str = 'In-hospital_death') -> None:
        """
        Sets data and target from the data available through data factory.

        Parameters
        ----------
        target_variable_name : str
            The name of the column which includes the target variable
        """
        self.target, self.data = self._set_data(self.data_factory, target_variable_name)

    def _set_data(self, data_factory: DataFactory, target_variable_name: str) -> [pd.Series, pd.DataFrame]:
        data = data_factory.get_pipeline_data(PipelineStep.EXTRACT_FEATURES)
        target = data[target_variable_name]
        try:
            data = data.drop([target_variable_name, ID_REPR], axis=1)
        except KeyError:
            print(f'Model evaluation object expected id column, but found none, ignoring for now.')
            data = data.drop([target_variable_name], axis=1)
        return target, self._scale_data(data)

    def _scale_data(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Applies the scale transformation defined by self.scaler to self.X.
        """
        self.scaler.fit(data)
        columns = data.columns
        return pd.DataFrame(self.scaler.transform(data), columns=columns)

    def select_features(self, score_func=chi2) -> None:
        """
        Reduces the set of features to self.number_of_features through the given scoring function

        Parameters
        ----------
        score_func : func
            A scikit-learn compatible scoring function taking two parameters:
            X: {array-like, sparse matrix} of shape (n_samples, n_features)
                Sample vectors.
            y: array-like of shape (n_samples,)
                Target vector (class labels).
            and returning
            array, shape = (n_features,)
                score for each feature in given order
        """
        self.data = self._select_features(score_func, self.data, self.target)

    def _select_features(self, score_func: Callable, data: pd.DataFrame, target: pd.Series):
        selector = SelectKBest(score_func=score_func, k=self.number_of_features)
        selector.fit(data, target)
        selected_features = data.columns[selector.get_support()]
        return data[selected_features]

    @abstractmethod
    def run(self, n_jobs: int = -1) -> None:
        """
        Performs the actual model evaluation using multiple processes, if desired.

        Parameters
        ----------
        n_jobs : int
            The number of processes to be used, for the number of available processors use -1.
        """
        pass

    def sample_data(self, sampler: BaseSampler):
        """
        Samples the data for the current iteration according to the given sampler

        Parameters
        ----------
        sampler : BaseSampler
            sampler used for balancing
        """
        self.X, self.y = sampler.fit_resample(self.X, self.y)
        self.y = to_categorical(self.y)

    def fit_model(self, parameters: Mapping[str, Any], model_representation: str,
                  use_cache: bool = False) -> pd.Series:
        """
        Scikit-learn compatible method to fit a model defined by the given parameters and the given data

        Parameters
        ----------
        parameters : Mapping[str, list]
            Parameter map
        model_representation : str
            Representation of the model
        use_cache : bool
            Whether or not to save the result to disk for later use

        Returns
        -------
        str
            Log of result
        """
        model = EnsembleClassifier(self.ensemble_size, parameters)
        result = self.evaluate_model(model_representation, model)
        if use_cache:
            self.data_factory.cache_single_evaluation_search_result(result, model_representation)

        # log info and duration
        score = result['score']
        info(f'{model_representation}: {score:.3f}')
        return result

    def evaluate_model(self, model_representation: str, model: EnsembleClassifier) -> pd.Series:
        """
        Evaluates a given model on a given set of data.

        Parameters
        ----------
        model_representation : str
            Name of the model
        model : EnsembleClassifier
            Model to be evaluated

        Returns
        -------
        pd.Series
            Evaluation results
        """
        # fit and predict
        before = time.time()
        model.fit(self.X, self.y, epochs=2000, batch_size=256, verbose=0)
        y_prob = model.predict_probabilities(self.X_test)
        y_pred = y_prob.argmax(axis=-1)
        after = time.time()
        duration = after - before

        # evaluate
        result = pd.Series()
        result['model'] = model_representation
        result['duration'] = duration
        result['in_ensemble_difference'] = model.in_ensemble_difference
        result = result.append(get_metrics(self.y_test, y_pred))
        result = result.append(get_parameter_value_pairs_from_string(model_representation, self.parameter_map))
        return result
