from enum import Enum
from logging import warning
from typing import NamedTuple, Iterable


class ParameterKey(NamedTuple):
    name: str
    abbreviation: str


class DefaultParameterKeys(ParameterKey, Enum):
    BALANCE_STRATEGY = ('balance_strategy', 'ba')


class Parameter(NamedTuple):
    """
    Representation of a parameter
    """
    key: ParameterKey
    values: list


class ParameterMap:
    """
    Convenience class that offers the organization of name, abbreviation, value mappings making use of the custom class
    Parameter (a named tuple).

    Parameters
    ----------
    parameters : Iterable[Parameter]
        Parameters to be added to the map
    """

    def __init__(self, parameters: Iterable[Parameter]):
        self.abbreviation_name_map = {}
        self.name_abbreviation_map = {}
        self.name_values_map = {}
        for parameter in parameters:
            self.add_parameter(parameter)

    def add_parameter(self, parameter: Parameter) -> None:
        """
        Adds a parameter to the map.

        Parameters
        ----------
        parameter : Parameter
            Parameter to be added
        """
        if parameter.key.abbreviation in self.name_abbreviation_map.keys() and (
                self.name_abbreviation_map[parameter.key.name] != parameter.key.abbreviation or
                self.name_values_map[parameter.key.name] != parameter.values):
            raise AttributeError(f'Parameter cannot be added, because it would cause an ambiguous state')

        self.abbreviation_name_map[parameter.key.abbreviation] = parameter.key.name
        self.name_abbreviation_map[parameter.key.name] = parameter.key.abbreviation
        self.name_values_map[parameter.key.name] = parameter.values

    def get_abbreviation(self, name: str) -> str:
        """
        Given a name of a parameter, returns its abbreviation.

        Parameters
        ----------
        name : str
            Name of the parameter

        Returns
        -------
        str
            Abbreviation of the parameter
        """
        return self.name_abbreviation_map[name]

    def get_name(self, abbreviation) -> str:
        """
        Given a abbreviation of a parameter, returns its name.

        Parameters
        ----------
        abbreviation : str
            Abbreviation of the parameter

        Returns
        -------
        str
            Name of the parameter
        """
        return self.abbreviation_name_map[abbreviation]

    def get_values(self, key) -> list:
        """
        Given a name or abbreviation of a parameter, returns its values.

        Parameters
        ----------
        key : str
            Name or abbreviation of the parameter

        Returns
        -------
        list
            Values of the parameter
        """
        try:
            return self.name_values_map[key]
        except KeyError:
            try:
                return self.name_values_map[self.abbreviation_name_map[key]]
            except KeyError:
                warning(f'Could not find any values for "{key}".')
                return []
