from logging import debug
from typing import Mapping, Any

import numpy as np
from numpy.core.multiarray import ndarray
from sklearn.base import BaseEstimator
from sklearn.base import ClassifierMixin

from physionetChallenge.src.NeuralNetwork import NeuralNetwork, get_callbacks


class EnsembleClassifier(BaseEstimator, ClassifierMixin):
    """
    Ensemble classifier for scikit-learn estimators, inspired by
    https://sebastianraschka.com/Articles/2014_ensemble_classifier.html

    Parameters
    ----------
    number_of_estimators : int
        Ensemble size
    parameters : Mapping[str, list]
        Parameters for the models
    """

    def __init__(self, number_of_estimators: int, parameters: Mapping[str, Any], use_tensorboard: bool = False):
        self.classifiers = {}
        for i in range(number_of_estimators):
            self.classifiers[f'estimator_{i + 1}of{number_of_estimators}'] = NeuralNetwork(**parameters)
        self.use_tensorboard = use_tensorboard
        self.classes_ = []
        self.probabilities_ = []

        # needed later in prediction methods
        self.in_ensemble_difference = 0

    def fit(self, X: ndarray, y: ndarray, **kwargs) -> None:
        """
        Fit the scikit-learn estimators.

        Parameters
        ----------
        X : numpy array, shape = [n_samples, n_features]
            Training data
        y : list or numpy array, shape = [n_samples]
            Class labels
        kwargs :
            any additional parameters that are meant to be applied to the fit method
        """
        for name, classifier in self.classifiers.items():
            debug(f'fitting {name}...', end='\r')
            if self.use_tensorboard:
                classifier.fit(X, y, callbacks=get_callbacks(name), **kwargs)
            else:
                classifier.fit(X, y, callbacks=get_callbacks(), **kwargs)

    def predict(self, X: ndarray) -> ndarray:
        """
        Predict labels for test data.

        Parameters
        ----------
        X : numpy array, shape = [n_samples, n_features]
            Test data
        Returns
        -------
        list or numpy array, shape = [n_samples]
            Predicted class labels by majority rule
        """
        self.classes_ = np.array([classifier.predict(X) for _, classifier in self.classifiers.items()])
        if self.classes_[0].dtype == 'float64':
            self.classes_ = np.array([each.argmax(axis=-1) for each in self.classes_])
        n_samples = X.shape[0]
        n_classifiers = len(self.classifiers)
        self.in_ensemble_difference = np.abs(np.diff(self.classes_, axis=0)).sum() / (n_classifiers - 1)
        self.classes_ = self.classes_.reshape(n_classifiers, n_samples)
        maj = np.array([np.argmax(np.bincount(self.classes_[:, c])) for c in range(self.classes_.shape[1])])
        return maj

    def predict_probabilities(self, X: ndarray) -> ndarray:
        """
        Predict probabilities for test data

        Parameters
        ----------
        X : numpy array, shape = [n_samples, n_features]
            Test data
        Returns
        -------
        list or numpy array, shape = [n_samples, n_probabilities]
            Weighted average probability for each class per sample.
        """
        self.probabilities_ = np.array([classifier.predict(X) for name, classifier in self.classifiers.items()])
        n_classifiers = len(self.classifiers)
        self.in_ensemble_difference = np.abs(np.diff(self.probabilities_, axis=0)).sum() / (n_classifiers - 1)
        return np.average(self.probabilities_, axis=0)
