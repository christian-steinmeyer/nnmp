from enum import Enum


class VariableCategory(Enum):
    """
    Representation of the four variable categories, defined in Yun Chen and Hui Yang, “Heterogeneous post-surgical data
    analytics for predictive modeling of mortality risks in intensive care units”.

    (see http://www.ncbi.nlm.nih.gov/pubmed/25570946)
    """

    GENERAL_DESCRIPTOR = ('gd', 'General descriptor')
    LOW_SAMPLING_VARIABLE = ('low', 'Low-Sampling-Variable')
    MED_SAMPLING_VARIABLE = ('med', 'Med-Sampling-Variable')
    HIGH_SAMPLING_VARIABLE = ('high', 'High-Sampling-Variable')

    def __init__(self, abbreviation: str, description: str) -> None:
        self.abbreviation = abbreviation
        self.description = description

    def __repr__(self):
        return f'<{self.__class__.__name__}.{self.name}>'
