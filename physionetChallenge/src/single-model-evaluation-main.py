import logging
from multiprocessing import freeze_support

from physionetChallenge.src.GridSearch import *
from physionetChallenge.src.NeuralNetwork import NeuralNetworkParameterKeys
from physionetChallenge.src.ParameterMap import ParameterMap, Parameter
from physionetChallenge.src.SingleModelEvaluation import SingleModelEvaluation


def perform_model_evaluation():
    # number of features
    n_features = 20
    parameters = [Parameter(NeuralNetworkParameterKeys.NETWORK_DEPTH, [5]),
                  Parameter(NeuralNetworkParameterKeys.ACTIVATION_FUNCTION, ['relu']),
                  Parameter(NeuralNetworkParameterKeys.OPTIMIZER, ['adam']),
                  Parameter(NeuralNetworkParameterKeys.DROP_RATE, [0.1]),
                  Parameter(NeuralNetworkParameterKeys.INITIALIZER, ['RandomNormal']),
                  Parameter(NeuralNetworkParameterKeys.NETWORK_WIDTH, [n_features]),
                  Parameter(NeuralNetworkParameterKeys.NETWORK_DEPTH.INPUT_DIMENSIONS, [n_features]),
                  Parameter(NeuralNetworkParameterKeys.LAST_ACTIVATION_FUNCTION, ['softmax'])]

    parameter_map = ParameterMap(parameters)

    evaluation = SingleModelEvaluation(number_of_features=n_features, parameter_map=parameter_map,
                                       training_data_factory=DataFactory(data_set='a', project='physionetChallenge'),
                                       test_data_factory=DataFactory(data_set='b', project='physionetChallenge'),
                                       balance_strategy=RandomUnderSampler)
    evaluation.set_data(target_variable_name='In-hospital_death')
    evaluation.select_features()
    evaluation.run()


if __name__ == '__main__':
    logging.basicConfig(level='INFO')
    freeze_support()
    perform_model_evaluation()
