import math
import os
from textwrap import wrap
from typing import List

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from constants import ROOT_DIR
from physionetChallenge.src.ParameterMap import Parameter, ParameterKey
from physionetChallenge.src.Variable import Variable

plt.rcParams["figure.dpi"] = 300
plt.rcParams["font.size"] = 8
plt.rcParams['font.family'] = 'serif'


def set_plot_pane(width, height):
    plt.rcParams['figure.figsize'] = [width, height]


class PlotFactory:
    """
    Convenience class offering access to various plotting facilities.

    Parameters
    ----------
    data : pd.DataFrame
        Data to be plotted
    setting: str
        Setting, in which plots are created, e.g. 'preprocessing'
    """

    plot_path = f'{ROOT_DIR}/plots/'

    def __init__(self, data: pd.DataFrame, setting: str = None) -> None:
        self.data = data
        self._adjust_data()
        if setting is not None:
            self.plot_path = f'{self.plot_path}/{setting}'

    def save_figure(self, name: str) -> None:
        """
        Saves a copy of the current figure to disk.

        Parameters
        ----------
        name : str
            Name of the current figure to be used for file name
        """
        if not os.path.exists(self.plot_path):
            os.makedirs(self.plot_path)
        file_name = ''.join(name.split()).lower()
        plt.savefig(os.path.join(self.plot_path, f'{file_name}.png'))

    def plot_distributions(self, variables: List[Variable] = None, attribute: str = '',
                           save_plots: bool = False) -> None:
        """
        Creates distribution plots (histograms) for all given variables. If none are given, all original variables are
        used instead.

        Parameters
        ----------
        variables : List[Variable]
            All variables the distribution of which should be plotted
        attribute : str
            Additional attribute to describe the plotted data
        save_plots : bool
            If True, will save plots to disk
        """
        set_plot_pane(3, 3)
        if not variables:
            variables = Variable.get_original_variables()
        for variable in variables:
            fig, ax = plt.subplots()
            self.data[variable.label].hist(bins=100, log=True)
            wrapped_title = "\n".join(wrap(variable.full_name, 24))
            ax.set(xlabel='Value', ylabel='Count', title=wrapped_title)
            plt.tight_layout()
            if save_plots:
                figure_name = 'distribution-'
                if attribute is not None:
                    figure_name = f'{figure_name}{attribute}-'
                self.save_figure(f'{figure_name}{variable.full_name}')
            plt.show()

    def boxplot_summary(self, parameters: List[ParameterKey], title: str = None, y: str = 'score',
                        ylim: float = None, save_plots: bool = False) -> None:
        """
        Creates a combination of sub plots, each presenting a boxplot for the distribution of one of the parameters
        with respect to the target y.

        Parameters
        ----------
        parameters : List[ParameterKey]
            List of parameters to be plotted in individual sub plots
        title : str
            If present, is used to save plot to disk
        y : str
            Parameter name used for y axis
        ylim : float
            Limits the y axis visible range
        save_plots : bool
            If True, will save plots to disk
        """

        number_of_sub_figures = len(parameters)
        number_of_sub_figures_per_row = min(number_of_sub_figures, 4)
        number_of_sub_figure_rows = math.ceil(number_of_sub_figures / number_of_sub_figures_per_row)
        set_plot_pane(2 * number_of_sub_figures_per_row, 2 * number_of_sub_figure_rows)
        fig, axs = plt.subplots(number_of_sub_figure_rows, number_of_sub_figures_per_row, sharey='row')
        index = 0
        for parameter in parameters:
            row = index // number_of_sub_figures_per_row
            column = index - row * number_of_sub_figures_per_row
            if number_of_sub_figure_rows > 1:
                axis = axs[row][column]
            else:
                axis = axs[column]
            sns.boxplot(x=parameter.name, y=y, hue=parameter.name, data=self.data, dodge=False, ax=axis,
                        fliersize=1, linewidth=1)
            axis.set(xlabel=parameter.name, ylabel=y.title(), ylim=ylim)
            axis.get_legend().remove()
            index += 1
        fig.suptitle(title)
        fig.tight_layout(rect=[0, 0, 1, 0.95])
        if save_plots:
            self.save_figure(title)
        plt.show()

    def correlation_summary(self) -> None:
        """
        Creates a correlation heat map of all relevant variables in available data.
        """

        set_plot_pane(12, 12)
        data = self.data.copy()

        # convert columns into applicable data types
        data['balance_strategy'] = data['balance_strategy'].astype('category').cat.codes
        data['drop_rate'] = pd.to_numeric(data['drop_rate'])
        data['hidden_layers'] = pd.to_numeric(data['hidden_layers'])
        data['units'] = pd.to_numeric(data['units'])
        data['in_ensemble_difference'] = pd.to_numeric(data['in_ensemble_difference'])

        corr = data.corr()
        sns.heatmap(corr, xticklabels=corr.columns.values, yticklabels=corr.columns.values)
        plt.show()

    def _adjust_data(self) -> None:
        """
        Handle edge cases with missing values.
        """
        if 'model' in self.data.columns:
            self.data.dropna(subset=['model'], inplace=True)
        if 'score' in self.data.columns:
            self.data.dropna(subset=['score'], inplace=True)
        # TODO include or replace for grid search? self.data = self.data.fillna('None')
