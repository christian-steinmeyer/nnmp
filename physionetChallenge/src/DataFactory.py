import os
from enum import Enum
from functools import total_ordering
from logging import info, warning
from typing import Iterable, Callable, List, Union, Tuple

import numpy as np
import pandas as pd
from pandas.errors import EmptyDataError

from constants import ROOT_DIR, TIME_REPR, ID_REPR


def extract_info_from_row(case_id: int, row: pd.Series) -> dict:
    """
    Extracts information from a row of the physionet challenge data files

    Parameters
    ----------
    case_id : int
        Id of the case (identifier of the file)
    row : pd.Series
        Row of a data file being read by pandas

    Returns
    -------
    dict
        Dictionary containing the id, parameter-value-pair, and the time
    """
    time_str = row['Time']
    time = pd.to_timedelta(f'{time_str}:00')
    parameter = row['Parameter']
    value = row['Value']
    return {ID_REPR: case_id, parameter: value, TIME_REPR: time}


def _identity(series: pd.Series) -> pd.Series:
    return ~series.isna()


def _detect_outliers_based_on_x_std(series: pd.Series, x: int) -> pd.Series:
    return np.abs(series - series.mean()) > (x * series.std())


def _detect_outliers_based_on_5_std(series: pd.Series) -> pd.Series:
    return _detect_outliers_based_on_x_std(series, 5)


def _detect_outliers_based_on_3_std(series: pd.Series) -> pd.Series:
    return _detect_outliers_based_on_x_std(series, 3)


class OutlierStrategy(Enum):
    """
    Representation of different outlier detection strategies.
    """
    STD3 = ('std3', 'Detect outliers that are further than three times the standard deviation from the mean',
            _detect_outliers_based_on_3_std)
    STD5 = ('std5', 'Detect outliers that are further than five times the standard deviation from the mean',
            _detect_outliers_based_on_5_std)

    def __init__(self, abbreviation: str, description: str,
                 detection_function: Callable[[pd.Series], pd.Series]) -> None:
        self.abbreviation = abbreviation
        self.description = description
        self.predict = detection_function


@total_ordering
class PipelineStep(Enum):
    """
    Representation of different preprocessing steps.
    """
    MERGE_RAW = (1, 'raw', 'Merging of raw original data')
    REMOVE_ERRORS = (2, 'errors', 'Removing of erroneous data')
    APPLY_TRANSFORMATIONS = (3, 'transformations', 'Transforming selected variables')
    APPLY_IMPUTATIONS = (4, 'imputations', 'Imputing missing and absent data')
    REMOVE_OUTLIERS = (5, 'outliers', 'Removing outliers in the data')
    EXTRACT_FEATURES = (6, 'features', 'Creating features from variables')
    SELECT_FEATURES = (7, 'selections', 'Selecting relevant features')

    def __init__(self, step: int, label: str, description: str) -> None:
        self.step = step
        self.label = label
        self.description = description

    def __lt__(self, other: 'PipelineStep') -> bool:
        if self.__class__ is other.__class__:
            return self.step < other.step
        return NotImplemented

    @classmethod
    def get_all_before(cls, other: 'PipelineStep') -> List['PipelineStep']:
        return [cls[member] for member in cls.__members__ if cls[member].step < other.step]

    @classmethod
    def get_next_step(cls, current_step: 'PipelineStep') -> Union[None, 'PipelineStep']:
        for member in cls.__members__:
            if cls[member].step == current_step.step + 1:
                return cls[member]
        return None


class DataFactory:
    """
    Convenience class that enables standardized access to data for this project.

    Parameters
    ----------
    outlier_strategy : OutlierStrategy
       Outlier strategy
    data_set : str
        String identifier of a dataset (single letter)
    """

    def __init__(self, outlier_strategy: OutlierStrategy = None, project: str = None, data_set: str = None):
        info(f'Initializing Data Factory...')
        if project is not None:
            self.original_data_path = f'{ROOT_DIR}/{project}/original_data'
            self.data_path = f'{ROOT_DIR}/{project}/data'
        else:
            self.original_data_path = f'{ROOT_DIR}/original_data'
            self.data_path = f'{ROOT_DIR}/data'
        self.outlier_strategy = outlier_strategy
        self.data_set = data_set
        self.current_pipeline_step = self._get_most_advanced_executed_pipeline_step()
        if self.current_pipeline_step is PipelineStep.MERGE_RAW:
            self._prepare_raw_data()
        try:
            self.grid_search_data = self.get_grid_search_data()
        except FileNotFoundError:
            self.grid_search_data = pd.DataFrame(columns=['model'])

    def get_outcome_file(self) -> pd.DataFrame:
        """
        For the selected data set, returns a data frame with the outcome data.

        Returns
        -------
        pd.DataFrame
            Outcome data for current data set.
        """
        return pd.read_csv(f'{self.original_data_path}/Outcomes-{self.data_set}.txt')

    def get_file(self, file_name, consider_outlier_strategy=True) -> pd.DataFrame:
        try:
            df = pd.read_csv(self._get_file_name(file_name, consider_outlier_strategy=consider_outlier_strategy))
        except EmptyDataError:
            # A file was found, but it was empty
            df = pd.DataFrame()
        if TIME_REPR in df.columns:
            df[TIME_REPR] = pd.to_timedelta(df[TIME_REPR])
        return df

    def get_raw_data(self) -> pd.DataFrame:
        """
        Retrieves raw data for the current data set, which has to be created first by self.prepare_raw_data.

        Returns
        -------
        pd.DataFrame
            Raw data
        """
        if not self._finds_executed_pipeline_data(PipelineStep.MERGE_RAW):
            self._prepare_raw_data()
        return self.get_file('raw', consider_outlier_strategy=False)

    def get_grid_search_data(self) -> pd.DataFrame:
        """
        Retrieves result data from grid search if executed prior to call.

        Returns
        -------
            Grid search results
        """
        return self.get_file('grid_search')

    def load_previous_data(self) -> Tuple[PipelineStep, pd.DataFrame]:
        """
        Retrieves the the data from the most advanced pipeline step executed before.
        Returns
        -------
        Tuple[PipelineStep, pd.DataFrame]
            Next pipeline step, corresponding stored data
        """
        info(f'... for {self.current_pipeline_step}')
        next_step = PipelineStep.get_next_step(self.current_pipeline_step)
        return next_step, self.get_pipeline_data(self.current_pipeline_step)

    def get_pipeline_data(self, step: PipelineStep = PipelineStep.MERGE_RAW) -> pd.DataFrame:
        """
        Retrieves preprocessed data if created prior to call.

        Returns
        -------
            Preprocessed data
        """
        return self.get_file(step.label)

    def save_to_file(self, df: pd.DataFrame, file_name: str, consider_outlier_strategy: bool = True) -> None:
        """
        Saves a data frame to disc.

        Parameters
        ----------
        df : pd.DataFrame
            Data frame to be saved
        file_name : str
            File name to be saved under
        consider_outlier_strategy : bool
            If true, outlier strategy will be appended to file name
        """
        df.to_csv(self._get_file_name(file_name, consider_outlier_strategy=True), sep=',', encoding='utf-8',
                  index=False)

    def save_pipeline_data(self, df: pd.DataFrame, step: PipelineStep) -> None:
        """
        Saves the given data frame as preprocessed data for later use.

        Parameters
        ----------
        step : PipelineStep
            Most recent executed pipeline step
        df : pd.DataFrame
            Processed data
        """
        self.save_to_file(df, step.label)

    def save_grid_search_data(self, df: pd.DataFrame):
        """
        Saves the given data frame as grid search result data for later use.

        Parameters
        ----------
        df : pd.DataFrame
            Intermediate grid search result
        """
        try:
            previous_results = self.get_file('grid_search')
        except FileNotFoundError:
            previous_results = pd.DataFrame()
        full_results = previous_results.append(df, sort=True)
        self.save_to_file(full_results, 'grid_search')

    def cache_single_evaluation_search_result(self, result: pd.Series, model_representation: str) -> None:
        """
        Saves the given data frame as intermediate grid search result data for later use to its own temporary file.

        Parameters
        ----------
        result : pd.Series
            Intermediate grid search result
        model_representation: str
            Identification for grid search iteration
        """
        data = pd.DataFrame(result).T
        self.save_to_file(data, 'grid_search_tmp-' + model_representation)

    def clean_grid_search_cache(self, model_representations: Iterable[str]):
        """
        Moves the gird search result from temporary files into collective one.

        Parameters
        ----------
        model_representations: Iterable[str]
            Identification for grid search iteration
        """
        intermediate_results = pd.DataFrame()
        for model_representation in model_representations:
            try:
                model_result = self.get_file('grid_search_tmp-' + model_representation)
                intermediate_results = intermediate_results.append(model_result)
            except FileNotFoundError:
                warning(f'The grid search cache file for "{model_representation}" could not be found')
        self.save_grid_search_data(intermediate_results)
        for model_representation in model_representations:
            try:
                os.remove(self._get_file_name(model_representation, prefix='grid_search_tmp'))
            except FileNotFoundError:
                continue

    def model_is_already_evaluated(self, model_representation: str) -> bool:
        """
        Returns whether or not a given model_representation is already evaluated.

        Parameters
        ----------
        model_representation :
            Identification for grid search iteration
        Returns
        -------
            True, if there already exists a result for this representation
        """
        try:
            return self.grid_search_data['model'].str.contains(model_representation).any() or self._is_cached(
                model_representation)
        except KeyError:
            # grid search data does not contain 'model' column and therefore is likely empty
            return False or self._is_cached(model_representation)

    def _prepare_raw_data(self) -> None:
        """
        For the selected data set, combines the data from all according data files into one big file. Basically,
        restructures the given data.
        """

        info(f'... preparing raw data')
        df = []
        for case_id, filename in self._get_original_data_file_names().items():
            file = pd.read_csv(filename)
            for _, row in file.iterrows():
                df.append(extract_info_from_row(case_id, row))
        df = pd.DataFrame(df)
        df.drop(['RecordID'], axis=1, inplace=True)
        self.save_pipeline_data(df, PipelineStep.MERGE_RAW)

    def _get_original_data_file_names(self):
        directory = f'{self.original_data_path}/set-{self.data_set}/'
        result = {}
        for filename in os.listdir(directory):
            case_id = int(filename.split('.')[0])
            result[case_id] = directory + filename
        return result

    def _get_most_advanced_executed_pipeline_step(self) -> PipelineStep:
        for step in PipelineStep:
            if self._finds_executed_pipeline_data(step) and not self._finds_executed_pipeline_data(
                    PipelineStep.get_next_step(step)):
                return step
        return PipelineStep.MERGE_RAW

    def _finds_executed_pipeline_data(self, step: PipelineStep):
        return os.path.isfile(self._get_file_name(step.label))

    def _is_cached(self, model_representation: str):
        try:
            self.get_file('grid_search_tmp-' + model_representation)
            return True
        except FileNotFoundError or EmptyDataError:
            return False

    def _get_file_name(self, name: str, prefix: str = None, consider_outlier_strategy: bool = False) -> str:
        full_name = f'{self.data_path}/'
        if prefix is not None:
            full_name += f'{prefix}-'
        full_name += name

        if self.data_set is not None:
            full_name += f'-{self.data_set}'

        if consider_outlier_strategy and self.outlier_strategy is not None:
            full_name += f'_{self.outlier_strategy.abbreviation}'
        full_name += '.csv'
        return full_name
