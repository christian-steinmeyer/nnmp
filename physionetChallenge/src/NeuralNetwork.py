from enum import Enum
from typing import Union, List

import tensorflow as tf
from keras import Model
from keras import backend as keras
from keras.callbacks import EarlyStopping, TensorBoard, Callback
from keras.initializers import Initializer
from keras.layers import Activation, Dense, Dropout, Input
from keras.optimizers import Optimizer
from keras.utils.generic_utils import get_custom_objects
from tensorflow_core.python.keras.losses import Loss

from constants import ROOT_DIR
from physionetChallenge.src.ParameterMap import ParameterKey


class NeuralNetworkParameterKeys(ParameterKey, Enum):
    NETWORK_DEPTH = ('hidden_layers', 'hl')
    ACTIVATION_FUNCTION = ('activation', 'act')
    OPTIMIZER = ('optimizer', 'opt')
    DROP_RATE = ('drop_rate', 'dr')
    INITIALIZER = ('initializer', 'init')
    NETWORK_WIDTH = ('units', 'n')
    INPUT_DIMENSIONS = ('input_dim', 'size')
    LAST_ACTIVATION_FUNCTION = ('last_activation', 'l-act')


def f1_loss(y_true, y_pred):
    """
    F1 Score, according to https://en.wikipedia.org/wiki/F1_score
    """
    tp = keras.sum(keras.cast(y_true * y_pred, 'float'), axis=0)
    fp = keras.sum(keras.cast((1 - y_true) * y_pred, 'float'), axis=0)
    fn = keras.sum(keras.cast(y_true * (1 - y_pred), 'float'), axis=0)

    precision = tp / (tp + fp + keras.epsilon())
    recall = tp / (tp + fn + keras.epsilon())

    f1 = 2 * precision * recall / (precision + recall + keras.epsilon())
    f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
    return 1 - keras.mean(f1)


def log_sigmoid(x: tf.Tensor):
    """
    Log sigmoid activation function
    """
    return keras.log(keras.sigmoid(x))


# provide new activation to be callable by string
logsig_activation = Activation(log_sigmoid)
get_custom_objects().update({'logsig': logsig_activation})


def get_callbacks(model_name: str = None) -> List[Callback]:
    """
    Convenience method to receive an early stopping and tensorboard callback.

    Parameters
    ----------
    model_name :
        Directory for tensor board logs ./logs/model_name)
    Returns
    -------
    List[Callback]
        Early stopping callback and if model_name was given, also tensor board callback
    """
    result = [EarlyStopping(monitor='loss', min_delta=0.001, patience=20)]
    if model_name is not None:
        result.append(TensorBoard(log_dir=f'{ROOT_DIR}/logs/TensorBoard/{model_name}'))
    return result


class NeuralNetwork(Model):
    """
    Keras functional model with a certain width and length and other given parameters.

    Parameters
    ----------
    hidden_layers : int
        Number of hidden layers ('depth')
    activation : Union[str, Activation]
        Activation function or name of such for 'inner' layers
    last_activation : Union[str, Activation]
        Activation function or name of such for the last layer
    optimizer : Union[str, Optimizer]
        Optimizer or name of such
    drop_rate : float
        Percentage of data dropped at each hidden layer [0.0, 1.0]
    initializer : Union[str, Initializer]
        Initializer or name of such
    units : int
        Number of units per hidden layer ('width')
    input_dim : int
        Number of input values
    output_dim : int
        Number of output values
    loss : Union[str, Loss]
        Loss function or name of such
    """

    def __init__(self, hidden_layers: int = 1, activation: Union[str, Activation] = 'tanh',
                 last_activation: Union[str, Activation] = 'sigmoid', optimizer: Union[str, Optimizer] = 'adam',
                 drop_rate: float = 0.0, initializer: Union[str, Initializer] = 'RandomNormal', units: int = 40,
                 input_dim: int = -1, output_dim: int = 2, loss: Union[str, Loss] = f1_loss) -> None:
        self.hidden_layers = hidden_layers
        self.activation = activation
        self.last_activation = last_activation
        self.optimizer = optimizer
        self.drop_rate = drop_rate
        self.initializer = initializer
        self.units = units
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.loss = loss

        inputs = Input(shape=(input_dim,))
        outputs = Dense(units=units, kernel_initializer=initializer)(inputs)
        outputs = Activation(activation)(outputs)
        outputs = Dropout(rate=drop_rate)(outputs)
        for _ in range(hidden_layers - 1):
            outputs = Dense(units=units, kernel_initializer=initializer)(outputs)
            outputs = Activation(activation)(outputs)
            outputs = Dropout(rate=drop_rate)(outputs)
        outputs = Dense(units=output_dim, kernel_initializer=initializer)(outputs)
        if isinstance(last_activation, Activation):
            outputs = last_activation(outputs)
        else:
            outputs = Activation(last_activation)(outputs)
        super().__init__(inputs, outputs)
        self.compile(loss=loss, optimizer=optimizer, metrics=['binary_accuracy'])
