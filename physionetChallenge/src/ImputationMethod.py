from enum import Enum


class ImputationMethod(Enum):
    """
    Representation of the three imputation methods deduced from Yun Chen and Hui Yang, “Heterogeneous post-surgical data
    analytics for predictive modeling of mortality risks in intensive care units”.

    (see http://www.ncbi.nlm.nih.gov/pubmed/25570946)
    """

    ONE = 'Erroneous values are removed, and missing values were replaced using linear regression based on the most ' \
          'common values by gender.'
    FOUR = 'Missing values are imputed by a random value from the Gaussian distribution representing the normal ' \
           'physiology'
    REPLACED = 'Whole variable is replaced by another (potentially newly formed) variable.'

    def __repr__(self):
        return f'<{self.__class__.__name__}.{self.name}>: ({self.value})'