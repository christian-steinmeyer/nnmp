# Original Data

Include the original `.txt`-files from [the physionet challenge](https://www.physionet.org/challenge/2012/) in this folder (see [above readme](../README.md)).