# Original Data

Include the original `.csv`-files from [the MIMIC-III Database](https://physionet.org/works/MIMICIIIClinicalDatabase/files/) in this folder for the following tables:

- ADMISSIONS
- CHARTEVENTS
- D_ITEMS
- D_LABITEMS
- DIAGNOSES_ICD
- ICUSTAYS
- LABEVENTS
- PATIENTS
- TRANSFERS

Note, that the [existing file](VENTILATION_FIRSTDAY.csv) was created using the scripts [ventilation duration](https://github.com/MIT-LCP/mimic-code/blob/master/concepts/durations/ventilation-durations.sql) and [ventilation first day](https://github.com/MIT-LCP/mimic-code/blob/master/concepts/firstday/ventilation-first-day.sql) from the [original code repo on github](https://github.com/MIT-LCP/mimic-code/). It is included for robustness of the executable migrations due to naming etc. This might have to be overriden for future Datasets.