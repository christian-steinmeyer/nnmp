from data_factory import *
from keras.utils import to_categorical

from autokeras import MlpModule
from autokeras.backend.torch.loss_function import classification_loss
from autokeras.nn.metric import Accuracy
from autokeras.backend.torch import DataTransformerMlp

data_factory = DataFactory(balance_strategy='sub', outlier_strategy=None, scale_strategy=None)
X, test_X, y, test_y = data_factory.get_train_and_test_data()
y = to_categorical(y)
test_y = to_categorical(test_y)

# take GreedySearcher as an example, you can implement your own searcher and
# pass the class name to the CnnModule by search_type=YOUR_SEARCHER.
mlpModule = MlpModule(loss=classification_loss, metric=Accuracy, searcher_args={}, verbose=True)
data_transformer = DataTransformerMlp(X.values)
train_data = data_transformer.transform_train(X.values, y)
test_data = data_transformer.transform_test(test_X.values, test_y)
fit_args = {
    "n_output_node": 2,
    "input_shape": X.shape,
    "train_data": train_data,
    "test_data": test_data
}
mlpModule.fit(n_output_node=fit_args.get("n_output_node"),
              input_shape=fit_args.get("input_shape"),
              train_data=fit_args.get("train_data"),
              test_data=fit_args.get("test_data"),
              time_limit=1 * 60 * 60)