import os
import matplotlib.pyplot as plt
import seaborn as sns
import math

from data_factory import *


class PlotFactory:
    def __init__(self, file_name, data_type='evaluation'):
        self.data_type = data_type
        self.data = pd.DataFrame()
        for outlier_strategy, representation in outlier_strategies.items():
            try:
                data_factory = DataFactory(outlier_strategy=outlier_strategy, file_name=file_name)
                self.data = self.data.append(data_factory.data)
            except FileNotFoundError:
                self.data = self.data
        self.adjust_data()

    def get_full_path(self, file_name):
        directory = '../plots/' + self.data_type + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
        full_path = os.path.join(directory, f'{file_name}.png')
        return full_path

    def adjust_data(self):
        self.data.dropna(subset=['Model', 'Score'], inplace=True)
        if self.data_type == 'evaluation':
            self.extract_strategy_to_own_column(strategy_name='outlier_strategy', strategies=outlier_strategies)
            self.extract_strategy_to_own_column(strategy_name='balance_strategy', strategies=balance_strategies)
            self.set_index_but_retain_model_column()
        self.data = self.data.fillna('None')
        
    def split_model(self, model, abbreviations):
        # artifacts of old code, TODO remove when not needed anymore
        model = model.replace('l_act', 'l-act').replace('ba-', 'ba=')
        # ignore first index, because that is the model name only
        parameters = model.split('_')[1:]
        index = []
        values = []
        for parameter in parameters:
            parts = parameter.split('=')
            index.append(abbreviations[parts[0]])
            values.append(parts[1])
        return pd.Series(values, index=index)
        
    def split_names(self, abbreviations):
        extracted_data = self.data.apply(lambda x: self.split_model(x['Model'], abbreviations), result_type='expand', axis=1)
        self.data[extracted_data.columns] = extracted_data
        
    def filter(self, column, value):
        self.data = self.data[self.data[column] == value]

    def extract_strategy_to_own_column(self, strategy_name, strategies):
        self.data[strategy_name] = 'None'
        for strategy, representation in strategies.items():
            if strategy:
                self.data[strategy_name] = self.data.apply(
                    lambda x: strategy if representation in x['Model'] else x[strategy_name], axis=1)

    def set_index_but_retain_model_column(self):
        self.data['Base_Model'] = self.data.apply(lambda x: x['Model'].split('_')[0], axis=1)
        self.data['Model2'] = self.data['Model']
        self.data = self.data.set_index(['Model', 'Split'])
        self.data = self.data.sort_index()
        self.data['Model'] = self.data['Model2']
        self.data = self.data.drop(['Model2'], axis=1)
        
    def correlation_plot(self, data_filter, title=None):
        plt.rcParams['figure.figsize'] = [12, 12]
        column, value = data_filter
        plot_data = self.data[self.data[column] == value]
        features = plot_data[['Score', 'activation', 'balance_strategy', 'drop_rate', 'hidden_layers', 
                          'last_activation', 'units']].copy()
        features['activation'] = features['activation'].astype('category').cat.codes
        features['balance_strategy'] = features['balance_strategy'].astype('category').cat.codes
        features['last_activation'] = features['last_activation'].astype('category').cat.codes
        features['drop_rate'] = pd.to_numeric(features['drop_rate'])
        features['hidden_layers'] = pd.to_numeric(features['hidden_layers'])
        features['units'] = pd.to_numeric(features['units'])
        corr = features.corr()
        sns.heatmap(corr, xticklabels=corr.columns.values, yticklabels=corr.columns.values)
        if title:
            plt.savefig(self.get_full_path(title))

    def boxplot_summary(self, variables, data_filter, title=None, x='Model', hue='Model'):
        size = len(variables)
        row_size = min(size, 4)
        row_count = math.ceil(size / row_size)
        plt.rcParams['figure.figsize'] = [16, 4 * row_count]
        dodge = False if size > 1 else True
        visible = False if x == hue else True
        if self.data_type == 'evaluation':
            plot_data = self.data[self.data['Model'].isin(data_filter)]
            sharex = True
            ratios = [3] * row_size + [2] if size > 1 else [6, 1]
            gridspec_kw = {'width_ratios': ratios}
            reverse = False
        elif self.data_type == 'grid-search':
            column, value = data_filter
            plot_data = self.data[self.data[column] == value]
            sharex = False
            gridspec_kw = {}
            reverse = True
        fig, axs = plt.subplots(row_count, row_size + 1, sharey=True, sharex=sharex, gridspec_kw=gridspec_kw)
        for i in range(1, size + 1):
            row = (i-1) // row_size
            column = (i-1) - row * row_size
            if row_count > 1:
                axis = axs[row][column]
            else:
                axis = axs[column]
            if reverse:
                sns.boxplot(x=variables[i-1], y=x, hue=variables[i-1], data=plot_data, dodge=dodge, ax=axis)
            else:
                sns.boxplot(x=x, y=variables[i - 1], hue=hue, data=plot_data, dodge=dodge, ax=axis)
            axis.axes.get_xaxis().set_visible(visible)
            handles, labels = axis.get_legend_handles_labels()
            axis.get_legend().remove()
        if row_count > 1:
            for row in range(0, row_count):
                axs[row][row_size].set_visible(False)
            empty_sub_plots = size % row_size
            for column in range(0, empty_sub_plots - 1):
                axs[row_count - 1][row_size - column].set_visible(False)
        else:
            axs[row_size].set_visible(False)
        if not reverse:
            fig.legend(handles, labels, loc='center right')
        if title:
            plt.savefig(self.get_full_path(title))
