import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans, Birch

from keras.layers import Lambda, Input, Dense
from keras.models import Model
from keras.losses import mse
from keras import backend as K

outlier_strategies = {
    None: '',
    '4std': '_ou-4std'
}

balance_strategies = {
    None: '',
    'mixed':'_ba-mixed',
    'sub': '_ba-sub',
    'super': '_ba-super',
    'k-means-centroid-sub': '_ba-kmcsub',
    'k-means-medioid-sub': '_ba-kmmsub',
    'birch-medioid-sub': '_ba-birmsub',
    'k-means-weighted-representatives-sub': '_ba-kmwrsub',
    'birch-weighted-representatives-sub': '_ba-birwrsub',
    'k-means-both-weighted-representatives-sub': '_ba-kmbwrsub'
#     'variational_auto_encoder-super': '_ba-vaesuper'
}

scale_strategies = {
    None: '',
    'standard': '_sc-std'
}

possible_target_variables = ['in_hospital_mortality', '30_day_mortality', '1_year_mortality']
target_variable = possible_target_variables[0]

def get_distance(a, b):
    return np.linalg.norm(a-b)

def get_medioids(df, labels, centroids = None):
    result = pd.DataFrame(columns=df.columns)
    for label in range(max(labels) + 1):
        candidates = [i for i, j in enumerate(labels) if j == label]
        if len(candidates) == 1:
            medioid = df.iloc[candidates[0]]
        else:
            if centroids is not None:
                centroid = centroids[label]
            else:
                centroid = df.iloc[candidates].mean(axis=0)
            medioid = centroid
            closest_distance = float('inf')
            for _, candidate in df.iloc[candidates].iterrows():
                distance = get_distance(centroid, candidate.values)
                if distance < closest_distance:
                    medioid = candidate
                    closest_distance = distance
        result = result.append(medioid)
    return result

def get_weighted_representatives(df, labels):
    number_of_samples = df.shape[0]
    number_of_clusters = max(labels) + 1
    result = pd.DataFrame(columns=df.columns)
    mini_cluster_labels = []
    for label in range(max(labels) + 1):
        candidates = [i for i, j in enumerate(labels) if j == label]
        size_of_cluster = len(candidates)
        if size_of_cluster == 1:
            mini_cluster_labels.append(label)
        else:
            number_of_draws = (number_of_clusters * size_of_cluster) / number_of_samples
            result = result.append(df.iloc[candidates].sample(frac=number_of_draws / size_of_cluster, random_state=42))
    candidates = [i for i, j in enumerate(labels) if j in mini_cluster_labels]
    size_of_cluster = len(candidates)
    number_of_draws = (number_of_clusters * size_of_cluster) / number_of_samples
    result = result.append(df.iloc[candidates].sample(frac=number_of_draws / size_of_cluster, random_state=42))      
    return result

def k_means_medioid_sub_sample(x, k):
    kmeans = KMeans(n_clusters=k, random_state=42, n_jobs=-1).fit(x)
    return get_medioids(x, kmeans.labels_, kmeans.cluster_centers_)

def k_means_centroid_sub_sample(x, k):
    kmeans = KMeans(n_clusters=k, random_state=42, n_jobs=-1).fit(x)
    return pd.DataFrame(kmeans.cluster_centers_, columns=x.columns)

def k_means_weighted_representative_sub_sample(x, k):
    labels = KMeans(n_clusters=k, random_state=42, n_jobs=-1).fit_predict(x)
    return get_weighted_representatives(x, labels)

def birch_medioid_sub_sample(x, k):
    labels = Birch(threshold=0.1, n_clusters=k).fit_predict(x)
    return get_medioids(x, labels)

def birch_weighted_representative_sub_sample(x, k):
    labels = Birch(threshold=0.1, n_clusters=k).fit_predict(x)
    return get_weighted_representatives(x, labels)

def variational_auto_encoder_super_sample(x, k):
    number_of_samples = k - x.shape[0]
    encoder, decoder, vae = create_variational_auto_encoder(x.shape[1])
    vae.fit(x,epochs=1000, shuffle=True, callbacks=early_stopping, batch_size=256, verbose=0)
    distributions = encoder.predict(x)[2]
    means = np.mean(distributions, axis=0)
    log_vars = np.log(np.var(distributions, axis=0))
    
    for i in range(number_of_samples):
        latent = [None] * 2
        latent[0] = np.random.normal(means[0], log_vars[0])
        latent[1] = np.random.normal(means[1], log_vars[1])
        sample = decoder.predict(np.array([latent]))
        x = x.append(pd.DataFrame(sample, columns=x.columns))
    return x


# directly from https://keras.io/examples/variational_autoencoder/
def sampling(args):
    """Reparameterization trick by sampling fr an isotropic unit Gaussian.

    # Arguments
        args (tensor): mean and log of variance of Q(z|X)

    # Returns
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean=0 and std=1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon


# see https://keras.io/examples/variational_autoencoder/ for details
def create_variational_auto_encoder(input_dim):
    # network parameters
    input_shape = (input_dim, )
    intermediate_dim = 512
    latent_dim = 2

    # VAE model = encoder + decoder
    # build encoder model
    inputs = Input(shape=input_shape, name='encoder_input')
    x = Dense(intermediate_dim, activation='relu')(inputs)
    z_mean = Dense(latent_dim, name='z_mean')(x)
    z_log_var = Dense(latent_dim, name='z_log_var')(x)

    # use reparameterization trick to push the sampling out as input
    z = Lambda(sampling, name='z')([z_mean, z_log_var])

    # instantiate encoder model
    encoder = Model(inputs, [z_mean, z_log_var, z], name='encoder')

    # build decoder model
    latent_inputs = Input(shape=(latent_dim,), name='z_sampling')
    x = Dense(intermediate_dim, activation='relu')(latent_inputs)
    outputs = Dense(input_dim, activation='sigmoid')(x)

    # instantiate decoder model
    decoder = Model(latent_inputs, outputs, name='decoder')

    # instantiate VAE model
    outputs = decoder(encoder(inputs)[2])
    vae = Model(inputs, outputs, name='vae_mlp')

    # define loss functions                                    
    reconstruction_loss = mse(inputs, outputs)
    reconstruction_loss *= input_dim
    kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
    kl_loss = K.sum(kl_loss, axis=-1)
    kl_loss *= -0.5
    vae_loss = K.mean(reconstruction_loss + kl_loss)
    vae.add_loss(vae_loss)
    
    vae.compile(optimizer='adam')
    return encoder, decoder, vae


class DataFactory:
    def __init__(self, balance_strategy=None, outlier_strategy=None, scale_strategy=None, file_name='TRAINING', sample_frac=1):
        self.balance_strategy = balance_strategy
        self.outlier_strategy = outlier_strategy
        self.scale_strategy = scale_strategy
        try:
            self.data = self.load(file_name)
        except FileNotFoundError:
            self.data = pd.DataFrame(columns=['Iteration'])
        if file_name == 'TRAINING':
            self.data = self.data.sample(frac=sample_frac)
            self.X = self.scale(self.data.drop(possible_target_variables, axis=1))
            self.y = self.data[target_variable]

    def get_full_path(self, file_name):
        file_name += outlier_strategies[self.outlier_strategy]
        return f'../data/{file_name}.csv'

    def load(self, name):
        file_path = self.get_full_path(name)
        return pd.read_csv(file_path)

    def scale(self, data):
        if self.scale_strategy == 'standard':
            result = pd.DataFrame(StandardScaler().fit_transform(data), index = data.index)
            result.columns = data.columns
        else:
            result = data
        return result

    def get_minority_class_ratio(self):
        all = len(self.y)
        minority = self.y.sum()
        return minority / all

    def balance(self, x, y):
        x = x.assign(y=y)
        x_1 = x[x['y'] == 1]
        x_0 = x[x['y'] == 0]
        x_0, x_1 = self.get_balanced_data(x_0, x_1)
        new_x = pd.concat([x_0, x_1], axis=0, sort=False)
        new_y = new_x['y']
        new_x = new_x.drop(['y'], axis=1)
        return new_x, new_y

    def get_balanced_data(self, data_maj, data_min):
        if self.balance_strategy == 'sub':
            return data_maj.sample(frac=data_min.shape[0] / data_maj.shape[0], random_state=42), data_min
        elif self.balance_strategy == 'super':
            return data_maj, data_min.sample(frac=data_maj.shape[0] / data_min.shape[0], replace=True, random_state=42)
        elif self.balance_strategy == 'mixed':
            return data_maj.sample(frac=(2 * data_min.shape[0]) / data_maj.shape[0], random_state=42),                   data_min.sample(frac=data_maj.shape[0] / (2 * data_min.shape[0]), replace=True, random_state=42)
        elif self.balance_strategy == 'k-means-centroid-sub':
            return k_means_centroid_sub_sample(data_maj, data_min.shape[0]), data_min
        elif self.balance_strategy == 'k-means-medioid-sub':
            return k_means_medioid_sub_sample(data_maj, data_min.shape[0]), data_min
        elif self.balance_strategy == 'birch-medioid-sub':
            return birch_medioid_sub_sample(data_maj, data_min.shape[0]), data_min
        elif self.balance_strategy == 'k-means-weighted-representatives-sub':
            return k_means_weighted_representative_sub_sample(data_maj, data_min.shape[0]), data_min
        elif self.balance_strategy == 'birch-weighted-representatives-sub':
            return birch_weighted_representative_sub_sample(data_maj, data_min.shape[0]), data_min
        elif self.balance_strategy == 'k-means-both-weighted-representatives-sub':
            k = data_min.shape[0] // 2
            return k_means_weighted_representative_sub_sample(data_maj, k), k_means_weighted_representative_sub_sample(data_min, k)
        elif self.balance_strategy == 'variational_auto_encoder-super':
            return data_maj, variational_auto_encoder_super_sample(data_min, data_maj.shape[0])
        else:
            return data_maj, data_min

    def get_train_and_test_data(self, test_size=0.1):
        x_train, x_test, y_train, y_test = train_test_split(self.X, self.y, test_size=test_size, stratify=self.y,
                                                            random_state=42)
        return x_train, x_test, y_train, y_test

    def get_number_of_features(self):
        return len(self.data.columns) - 3
    
    def model_is_already_evaluated(self, model_name, split):
        try:
            iteration = model_name + ', ' + str(split)
            return self.data['Iteration'].str.contains(iteration).sum() > 0
        except FileNotFoundError:
            return False

    def save_model_evaluation_results(self, intermediate_results):
        file_name = 'RESULTS-MODEL-EVALUATION'
        try:
            previous_results = self.load(file_name)
            previous_results = previous_results.set_index(['Model', 'Split'])
        except FileNotFoundError:
            previous_results = pd.DataFrame()
        full_results = previous_results.append(intermediate_results)
        full_results.to_csv(self.get_full_path(file_name), sep=',', encoding='utf-8', index=True)
        
    def save_model_evaluation_quick_results(self, intermediate_results):
        file_name = 'RESULTS-MODEL-QUICK-EVALUATION'
        try:
            previous_results = self.load(file_name)
            previous_results = previous_results.set_index(['Model'])
        except FileNotFoundError:
            previous_results = pd.DataFrame()
        full_results = previous_results.append(intermediate_results, sort=True)
        full_results.to_csv(self.get_full_path(file_name), sep=',', encoding='utf-8', index=True)

    def save_grid_search_results(self, intermediate_results):
        file_name = 'RESULTS-GRID-SEARCH'
        try:
            previous_results = self.load(file_name)
        except FileNotFoundError:
            previous_results = pd.DataFrame()
        full_results = previous_results.append(intermediate_results, sort=True)
        full_results.to_csv(self.get_full_path(file_name), sep=',', encoding='utf-8', index=False)

    def get_configuration_representation(self):
        return balance_strategies[self.balance_strategy] + outlier_strategies[self.outlier_strategy]
