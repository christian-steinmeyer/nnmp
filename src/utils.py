import pandas as pd
import matplotlib as plt

from sklearn.metrics import confusion_matrix, roc_auc_score

from constants import BALANCE_STRATEGY_ABBREVIATION, BALANCE_STRATEGY_REPR, SPLIT_REPR
from physionetChallenge.src.ParameterMap import ParameterMap

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

plt.rcParams['figure.figsize'] = [3, 3]
plt.rcParams["figure.dpi"] = 300
plt.rcParams["font.size"] = 8
plt.rcParams['font.family'] = 'serif'


def infer_date_time(column):
    return pd.to_datetime(column, errors='coerce', format="%Y-%m-%d %H:%M:%S")


def infer_time_delta(column, unit='d'):
    return pd.to_timedelta(column, errors='coerce', unit=unit)


def get_cluster_for_icd9_diagnosis(code):
    if str(code)[0] is 'E':
        return 19
    if str(code)[0] is 'V':
        return 18
    ncode = int(str(code)[:3])
    if ncode < 140:
        return 1
    if ncode < 240:
        return 2
    if ncode < 280:
        return 3
    if ncode < 290:
        return 4
    if ncode < 320:
        return 5
    if ncode < 390:
        return 6
    if ncode < 460:
        return 7
    if ncode < 520:
        return 8
    if ncode < 580:
        return 9
    if ncode < 630:
        return 10
    if ncode < 680:
        return 11
    if ncode < 710:
        return 12
    if ncode < 740:
        return 13
    if ncode < 760:
        return 14
    if ncode < 780:
        return 15
    if ncode < 800:
        return 16
    if ncode < 999:
        return 17
    return float("NaN")


def join(x):
    return sorted(x.unique())


def get_metrics(y_test, y_pred, verbose=0):
    tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel()
    ras = roc_auc_score(y_test, y_pred)
    acc = (tp + tn) / (tp + tn + fp + fn)
    sen = tp / (tp + fn)
    spe = tn / (tn + fp)
    result = pd.Series()
    result['true_positives'] = tp
    result['true_negatives'] = tn
    result['false_positives'] = fp
    result['false_negatives'] = fn
    result['roc_auc_score'] = ras
    result['accuracy'] = acc
    result['sensitivity'] = sen
    result['specificity'] = spe
    result['score'] = min(sen, acc, ras)
    if verbose:
        print(result)
    return result


def multi_metric_score(y_test, y_pred):
    tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel()
    sensitivity = tp / (tp + fn)
    accuracy = (tp + tn) / (tp + tn + fp + fn)
    roc_auc = roc_auc_score(y_test, y_pred)
    return min(sensitivity, accuracy, roc_auc)


def get_model_representation(model_name, balance_strategy, split):
    """
    Creates a model representation incorporating the model's name, balance strategy and split.

    Parameters
    ----------
    model_name : str
        Name of the model
    balance_strategy : str
        Name of the balance strategy
    split : int
        Split Identifier (count)
    Returns
    -------
    str
        '[model_name]_ba=[balance_strategy]_split=[split]'
    """
    return f'{model_name}_ba={balance_strategy}_split={split}'


def get_parameter_value_pairs_from_string(model_representation: str, parameter_map: ParameterMap) -> pd.Series:
    """
    Gets the parameters value paris from a model representation.

    Parameters
    ----------
    model_representation : str
        A string in the format (_[abbreviation]=[value])*
    parameter_map : ParameterMap
        Parameter map containing abbreviation and name information

    Returns
    -------
    pd.Series
        parameter value pairs
    """
    # ignore first index, because that is only the model name or null
    parameter_pairs = model_representation.split('_')[1:]
    result = pd.Series()
    for parameter_pair in parameter_pairs:
        abbreviation, value = parameter_pair.split('=')
        if abbreviation == BALANCE_STRATEGY_ABBREVIATION:
            result[BALANCE_STRATEGY_REPR] = value
        elif abbreviation == SPLIT_REPR:
            result[SPLIT_REPR] = value
        else:
            result[parameter_map.get_name(abbreviation)] = value
    return result
