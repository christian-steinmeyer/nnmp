# Mortality Prediction with Neural Networks

## Installation

First, make sure to have [conda](https://conda.io/docs/index.html) installed and are familiar with its basic functionality. Then, run `conda env create -f environment.yml` from this directory on the *anaconda prompt* to create the environment. If you experience trouble while installation, try manually creating a new conda environment and installing the packages from [the environment-definition-file](environment.yml) one after the other.

To avoid clutter when using git with jupyter notebook, also run `nbstripout --install` from the activated conda environment.


## Usage

For convenience, this project comes with various `*main.py` scripts, which can be directly executed.

## Data

Make sure, you copied all relevant `.csv` files into the [original data folder](original_data/). Then, execute the migrations scripts in the following order:
Only then, can the scripts be executed properly.

Note, that they were separated into multiple files for better readability and performance due to the huge amount of data in some tables.

## mRMR
If you would like to recreate the results for mRMR feature select, please proceed as follows:

- Download an implementation of mRMR, e.g. from the [creator's website](http://home.penglab.com/proj/mRMR/)
- In case, you are working on a windows machine, you might need to install the corresponding Visual Studio Components (you can use [this config file](/mrmr-requirements.vsconfig))
- You'll need a preprocessed dataset (e.g. from executing `pre-processing-main`) containing feature data `X` and the corresponding target labels `y`
- Create a joined `.csv` file by using `y` as the first column
- Execute mRMR on your data choosing *MID* and no discretization threshold for default behavior
- Integrate resulting file as done below:

```python
# Define custom score function to use mrmr results
columns = X.columns
tmp = pd.read_csv(f'./path/to/mrmr_results.csv', sep=',')
mrmr_results = {}
for index, row in tmp.iterrows():
    mrmr_results[row["Name"]] = row["Score"]

def read_from_mrmr(X, y):
    result = []
    for column in columns:
        if column != "ID":
            result.append(mrmr_results[column])
    return np.asarray(result)

# Use from AbstractModelEvaluation, e.g. in GridSearch:
grid_search = GridSearch(...)
grid_search.set_data(target_variable_name=some_target)
grid_search.select_features(score_func=read_from_mrmr)
```